Change Log
----------

Current release MetaData v0.0.0 (2016/??/??)
-------------------------------------------------

First alpha.

- first implementation of md::msg::Enumerate for container-like listing;
- modified Factory::describe() method return type from Descriptor pointer to
  Descriptor reference;
- made API docs;
- first binaries;
