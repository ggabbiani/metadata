Install
-------

Basic Installation (as root)
----------------------------

cd in your source directory (usually /usr/local/src) and untar:

    cd /usr/local/src
    tar -xzf <full path to release tarball>

make a build directory and cd into:

     mkdir build && cd build

configure the project:

     cmake .. -DCMAKE_BUILD_TYPE=MinSizeRel

compile:

     make

check:

     make test

install (as root):

     make install

Deinstallation (as root)
------------------------

    cd <build-directory>
    xargs rm < install_manifest.txt

Binary packages creation
------------------------

It is possible to build a binary package for the build architecture.

     make package

if everything proceeds as it should, you have an rpm / apm ready to be installed
