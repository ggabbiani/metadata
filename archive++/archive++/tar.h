/*
 * This file is part of archive++ project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * archive++ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with archive++.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ARCHIVEPP_TAR_H
#define ARCHIVEPP_TAR_H

#include <iterator>
#include <memory>

#include <archive.h>
#include <archive_entry.h>

namespace arc {

namespace tar {

struct Entry {
  std::string pathName;
};

class Reader {
public:
  struct Callback {
    virtual int open(struct archive *)                         = 0;
    virtual bool isOpen(struct archive *)                      = 0;
    virtual ssize_t read(struct archive *,const void* &buffer) = 0;
    virtual int close(struct archive *)                        = 0;
    std:: string fname;
  };
  Reader(Callback *cback);
  ~Reader();
  bool isOpen() {
    return callback_->isOpen(a_);
  }
  void open(const std::string &fname);
  bool nextHeader();
  void dataSkip();
  Entry& getEntry(Entry &entry) {
    entry.pathName = archive_entry_pathname(e_);
    return entry;
  }
  struct archive *a_ = nullptr;
private:
  struct archive_entry *e_ = nullptr;
  std::unique_ptr<Callback> callback_;
  void rise();
};

class Iterator : public std::iterator<std::input_iterator_tag, Entry> {
public:
  Iterator() {
  }
  Iterator(Reader &reader) {
    this->reader = reader.nextHeader() ? &reader : nullptr;
  }
  Iterator(const Iterator &other) : reader(other.reader) {
  }
  Iterator& operator () (Reader &reader) {
    this->reader = reader.nextHeader() ? &reader : nullptr;
    return *this;
  }
  Iterator &operator ++ () {
    reader->dataSkip();
    if (!reader->nextHeader()) {
      reader = nullptr;
    }
    return *this;
  }
  Iterator operator ++ (int) {
    Iterator tmp(*this);
    operator++();
    return tmp;
  }
  bool operator == (const Iterator& rhs) {
    return reader == rhs.reader;
  }
  bool operator != (const Iterator& rhs) {
    return reader != rhs.reader;
  }
  reference operator * () {
    reader->getEntry(entry);
    return entry;
  }
  const reference operator * () const {
    reader->getEntry(entry);
    return entry;
  }
  pointer operator -> () {
    reader->getEntry(entry);
    return &entry;
  }
  const pointer operator -> () const {
    reader->getEntry(entry);
    return &entry;
  }
private:
  Reader *reader = nullptr;
  mutable Entry   entry;
};

}

}

#endif // ARCHIVEPP_TAR_H
