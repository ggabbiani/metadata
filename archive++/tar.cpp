/*
 * This file is part of archive++ project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * archive++ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * archive++ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with archive++.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <eel/eel.h>

#include "archive++/tar.h"

using namespace std;
using namespace eel;

namespace {

int cbOpen(struct archive *a, void *client_data) {
  return dynamic_cast<arc::tar::Reader::Callback&>(*(arc::tar::Reader::Callback*)(client_data)).open(a);
}

ssize_t cbRead(struct archive *a, void *client_data, const void **buff) {
  return dynamic_cast<arc::tar::Reader::Callback&>(*(arc::tar::Reader::Callback*)(client_data)).read(a,*buff);
}

int cbClose(struct archive *a, void *client_data) {
  return dynamic_cast<arc::tar::Reader::Callback&>(*(arc::tar::Reader::Callback*)(client_data)).close(a);
}

}

namespace arc {

namespace tar {

Reader::Reader(Callback *cback) {
  this->callback_.reset(cback);
  if (!( a_ = archive_read_new())) {
    rise();
  }
}

Reader::~Reader() {
  if ( a_ ) {
    archive_read_free( a_ );
  }
}

bool Reader::nextHeader() {
  return (archive_read_next_header(a_,&this->e_) == ARCHIVE_OK);
}

void Reader::dataSkip() {
  archive_read_data_skip(a_);
}

void Reader::rise() {
  throw eel::RuntimeError(eel::attribute::what("tar archive error #" + boost::lexical_cast<string>(archive_errno( a_ )) + " - " + string(archive_error_string( a_ )) ));
}

void Reader::open(const string &fname) {
  this->callback_->fname = fname;
  archive_read_support_format_tar( a_ );
  if (archive_read_open( a_, this->callback_.get(), cbOpen, cbRead, cbClose) != ARCHIVE_OK) {
    rise();
  }
}

}

}
