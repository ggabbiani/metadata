/*
 * This file is part of archive++ project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * archive++ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with archive++.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <iostream>
#include <string>
#include <unistd.h>

#include "archive++/tar.h"

using namespace std;

namespace arc {

namespace tar {

namespace example_iterate {

struct MyCallback : public Reader::Callback {
  int open(struct archive *a) override {
    fd = ::open(fname.c_str(), O_RDONLY);
    return (fd >= 0 ? ARCHIVE_OK : ARCHIVE_FATAL);
  }
  ssize_t read(struct archive *a,const void* &buffer) override {
    buffer = this->buffer;
    return (::read(this->fd, this->buffer, bsize));
  }
  int close(struct archive *a) override {
    if (this->fd > 0)
      ::close(this->fd);
    return (ARCHIVE_OK);
  }
  bool isOpen(struct archive *a) override {
    return fd > -1;
  }
  constexpr static int bsize = 10240;
  int fd = -1;
  char buffer[bsize];
};

}

}

}

int main(int argc,const char *argv[]) {
  using namespace arc::tar::example_iterate;
  arc::tar::Iterator end;
  arc::tar::Reader reader(new MyCallback);
  reader.open(argv[1]);
  for (arc::tar::Iterator current(reader); current != arc::tar::Iterator(); ++current) {
    cout <<  current->pathName << endl;
  }
}

