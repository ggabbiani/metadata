cmake_minimum_required(VERSION 2.8)
project(metadata)
enable_testing()

###############################################################################
# extra modules
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/modules")

###############################################################################
# hosting system introspection
include(Introspection)

###############################################################################
# pre-reqs
find_package(Boost 1.53 REQUIRED)

find_library(EEL_LIBRARY eel)
if (NOT EEL_LIBRARY)
 MESSAGE(FATAL_ERROR "EEL library missing.")
endif ()
find_library(TSL_LIBRARY tsl)
if (NOT TSL_LIBRARY)
 MESSAGE(FATAL_ERROR "TSL library missing.")
endif ()
find_library(INTROSPECTIVE_LIBRARY introspective)
if (NOT INTROSPECTIVE_LIBRARY)
 MESSAGE(FATAL_ERROR "Introspective library missing.")
endif ()

###############################################################################
# project environment
# following two variables are available from cmake v3.3
# set(CMAKE_CXX_STANDARD_REQUIRED "ON")
# set(CMAKE_CXX_STANDARD "11")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(PROJECT_VERSION "0.0.2")
set(PORTAGE_GROUP "dev-libs")

include_directories(BEFORE
archive++
shared-objects/ascii
shared-objects/blockdevice
shared-objects/characterdevice
shared-objects/device
shared-objects/directory
shared-objects/elf
shared-objects/executable
shared-objects/fifo
shared-objects/md
shared-objects/regular_file
shared-objects/socket
shared-objects/symlink
shared-objects/system
shared-objects/tar
shared-objects/text
shared-objects/unix_socket
shared-objects/utf8
trace/include
)

###############################################################################
# Environment setup
set(MD_LIB_INSTALL_DIR ${DISTRO_LIB_INSTALL_DIR}/md)
set(MD_INC_INSTALL_DIR include/md)
# issue in GCC 4.7 for the std::this_thread::sleep_for() method and the _GLIBCXX_USE_NANOSLEEP definition
execute_process(COMMAND ${CMAKE_C_COMPILER} -dumpversion OUTPUT_VARIABLE GCC_VERSION)
if (GCC_VERSION VERSION_GREATER 4.6 AND GCC_VERSION VERSION_LESS 4.8)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D_GLIBCXX_USE_NANOSLEEP")
endif()
if (CMAKE_BUILD_TYPE STREQUAL "Debug")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -fsanitize=address")
endif()

###############################################################################
# environment setup for testing binaries: it's important to keep the following
# 3 lines BEFORE the unit-tests
set(MD_SOPATH "" CACHE STRING "TEST PATH" FORCE)
add_subdirectory(shared-objects)
configure_file(project.env.in project.env @ONLY)

# basics for Gentoo ebuild composition
configure_file ("${PROJECT_NAME}.ebuild.in" "${PROJECT_NAME}-${PROJECT_VERSION}.ebuild" @ONLY)
# file list for source tarball
configure_file ("src-list.txt.in" "src-list.txt" @ONLY)

###############################################################################
# subs
add_subdirectory(applications)
add_subdirectory(archive++)
add_subdirectory(data)
add_subdirectory(docs)
add_subdirectory(etc)
add_subdirectory(other)
add_subdirectory(trace)
add_subdirectory(third-part)
add_subdirectory(unit-tests)

###############################################################################
# package generator
SET(CPACK_GENERATOR "${DISTRO_CPACK_GENERATOR}")

# general properties
SET(CPACK_PACKAGE_VENDOR "giampiero@gabbiani.org")
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Metadata Library")
SET(CPACK_PACKAGE_DESCRIPTION_FILE "${PROJECT_SOURCE_DIR}/README.md")
set(CPACK_PACKAGE_CONTACT "Giampiero Gabbiani giampiero@gabbiani.org")
set(CPACK_PACKAGE_NAME "${PROJECT_NAME}")
set(CPACK_PACKAGE_VERSION "${PROJECT_VERSION}")

if (CPACK_GENERATOR STREQUAL "RPM" )
  message("++ RPM generator config")
  # bug for erroneous inclusion of system directories
  set(CPACK_RPM_SPEC_MORE_DEFINE "%define ignore \#")
  SET(CPACK_RPM_USER_FILELIST
  "%ignore /usr"
  "%ignore /usr/${DISTRO_LIB_INSTALL_DIR}"
  "%ignore /usr/include"
  "%ignore /usr/share"
  )
  # rpm properties
  SET(CPACK_RPM_PACKAGE_RELEASE "1${DISTRO_TAG}")
  SET(CPACK_RPM_PACKAGE_VERSION ${PROJECT_VERSION})
  SET(CPACK_RPM_PACKAGE_ARCHITECTURE ${DISTRO_ARCH})
  SET(CPACK_RPM_PACKAGE_LICENSE "GNU Lesser General Public License")
  SET(CPACK_RPM_PACKAGE_GROUP "System Environment/Libraries")
  SET(CPACK_RPM_PACKAGE_URL "http://www.gabbiani.org/mydigitalnotes/projects/22-introspective.html")
  set(CPACK_PACKAGE_FILE_NAME "${PROJECT_NAME}-${PROJECT_VERSION}-${CPACK_RPM_PACKAGE_RELEASE}.${DISTRO_ARCH}")
elseif(CPACK_GENERATOR STREQUAL "DEB")
  set(CPACK_PACKAGE_FILE_NAME "${CMAKE_PROJECT_NAME}_${PROJECT_VERSION}-${DISTRO_TAG}_${DISTRO_ARCH}")
  SET(CPACK_DEBIAN_PACKAGE_HOMEPAGE "http://www.gabbiani.org/mydigitalnotes/projects/22-introspective.html")
  set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "${DISTRO_ARCH}")
else (CPACK_GENERATOR STREQUAL "RPM" )
  set(CPACK_PACKAGE_FILE_NAME "${CMAKE_PROJECT_NAME}-${PROJECT_VERSION}${DISTRO_TAG}.${DISTRO_ARCH}")
endif (CPACK_GENERATOR STREQUAL "RPM" )

# platforms must be added AFTER proper CPACK related variables initialization
add_subdirectory(platforms)

#always the last...
INCLUDE(CPack)
