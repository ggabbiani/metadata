/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/program_options.hpp>
#include <eel/eel.h>
#include <introspective/proxy.h>
#include <iostream>
#include <memory>
#include <tsl/rtx.h>

#include <md/executable.h>
#include <md/md.h>
#include <md/properties.h>

#define DOTRACE
#include "debug/trace.h"

using namespace std;
using namespace eel;
using namespace md;
using namespace introspective;

namespace fs   = boost::filesystem;
namespace po   = boost::program_options;
namespace prop = md::property;

int main(int argc,char* argv[]) {
  TR_FUNC;
  int rc = EXIT_SUCCESS;
  try {
    string cmd_name(argv[0]);
    po::options_description desc(
      "Usage: " + cmd_name + " [OPTION]... FILES\n"
      "Determine file type of FILES.\n"
      "\nAvailable options"
    );
    desc.add_options()
    ("help,h",    "produce help message")
    ("verbose,v", "verbose description" )
    ("property,p", po::value<string>(), "read the property passed by name")
    ("exec,x",     po::value<string>(), "execute the object with the parameter string passed")
    ("files",      po::value< vector< string > >(), "file path list")
    ;
    po::positional_options_description pd;
    pd.add("files",-1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc,argv).options(desc).positional(pd).run(),vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return EXIT_SUCCESS;
    }

    if (vm.count("files")==0) {
      cout << desc << endl;
      return EXIT_FAILURE;
    }

    // retrieve the Factory instance
    Factory& factory = Factory::configure();
    // for each file given we - try - to extract info(s)
    for (auto i: vm["files"].as< vector< string > >()) {
      fs::path currentPath(i);
      // try to obtain the meta data object for the current file ...
      introspective::proxy::UniquePtr< Canonical > object(factory.create(currentPath));
      TR_MSG("object " << object.get());
      // test if recognized...
      if (!object) {
        cout << currentPath << " UNKNOWN." << endl;
        return EXIT_SUCCESS;
      }
      if (vm.count("property")) {
        Property &property = object [ msg::GetProperty(vm["property"].as<string>()) ];
        cout << *property.value << endl;
      } else if (vm.count("exec")) {
        rc = object [ md::executable::Execute(vm["exec"].as<string>()) ];
      } else if (!vm.count("verbose")) {
        cout << object->name() << ": " << object [msg::GetAs< string >(prop::DESCRIPTION)] << endl;
      } else {
        cout << "properties :\n";
        msg::Inspect::Result result = object [ msg::Inspect() ];
        for(auto i: result.properties) {
          cout << i << endl;
        }
        cout << "methods    :\n";
        for(auto i: result.methods) {
          cout << i << endl;
        }
      }
    }
  } catch(const eel::Exception& error) {
    cerr << error.what() << endl
         << "**** DIAGNOSTIC EXCEPTION DUMP ****\n"
         << error << endl
         << "***********************************" << endl;
  } catch(const exception& error) {
    cerr << error.what() << endl;
  } catch(...) {
    cerr << "this sould not occur...\n";
    return EXIT_FAILURE;
  }
  return rc;
}
