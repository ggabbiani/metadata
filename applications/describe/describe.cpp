/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/program_options.hpp>
// #include <cxxabi.h>
#include <eel/eel.h>
#include <iomanip>
#include <iostream>
#include <stdexcept>

#include "md/system.h"

using namespace std;
using namespace md;

namespace sys   = md::system;
namespace po    = boost::program_options;
namespace imsg  = introspective::msg;

vector<string> fnames;
// 
// option::Vector<> fileName      ("  FILE        : a file name",fnames);
// option::Switch        verbose  ("  -v,--verbose: verbose mode");
// option::Help          help     ("  -?,-h,--help: display this help and exit",
//   "Usage: file [OPTION]... FILE\n"
//   "Determine file type of FILES.\n",
//   "\n"
//   "When in verbose mode, the given compact string representation of the property's flags is the following:\n"
//   "p\tPUBLIC    - meta-class inheritable\n"
//   "o\tOBJECT    - the scope is object, meta-class otherwise\n"
//   "r\tREAD      - readable\n"
//   "w\tWRITE     - writeable\n"
//   "e\tREFERENCE - the property is a reference to a meta-class or object attribute\n"
// );

int main(int argc,char* argv[]) {
  po::options_description desc(
    "Usage: file [OPTION]... FILE\n"
    "Determine file type of FILES.\n"
    "\n"
    "When in verbose mode, the given compact string representation of the property's flags is the following:\n"
    "p\tPUBLIC    - meta-class inheritable\n"
    "o\tOBJECT    - the scope is object, meta-class otherwise\n"
    "r\tREAD      - readable\n"
    "w\tWRITE     - writeable\n"
    "e\tREFERENCE - the property is a reference to a meta-class or object attribute\n"
    "\nAvailable options"
    );
  try {
    desc.add_options()
      ("help,h", "produce help message")
      ("verbose,v","verbose mode")
      ("files",po::value<vector<string>>(),"a file name")
    ;
    po::positional_options_description pd;
    pd.add("files",-1);
    
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
      cout << desc << endl;
      return EXIT_SUCCESS;
    }

    if (vm.count("files")==0) {
      cout << desc << endl;
      return EXIT_FAILURE;
    }
    // retrieve the Factory instance
    Factory& factory = Factory::configure();
    // for each file given we - try - to extract info(s)
    for( int i=0;i<vm["files"].as<vector<string>>().size();i++ ){
      string currentFileName = vm["files"].as<vector<string>>()[i];
      //TR_MSG("currentFileName: " << currentFileName);
      // try to obtain the meta data descriptor for the current file ...
      Descriptor &descriptor = factory.describe(currentFileName);
      // test if recognized...
      if (vm["verbose"].empty()) {
        cout << descriptor.name() << ": " << descriptor [imsg::GetAs<string>("MD_DESCRIPTION")] << endl;
      } else {
        cout << "properties :\n";
        imsg::Inspect::Result result = descriptor [ imsg::Inspect() ];
        for(auto i: result.properties) {
          cout << i << endl;
        }
        cout << "methods    :\n";
        for(auto i: result.methods) {
          cout << i << endl;
        }
      }
    }
  } catch (const tsl::ParserError &error) {
    cerr << error.what() << endl
         << "**** DIAGNOSTIC EXCEPTION DUMP ****\n"
         << error << endl
         << tsl::parser::Helper(error) << endl
         << "***********************************" << endl;
  } catch(const eel::Exception& error) {
    cerr << error.what() << endl
         << "**** DIAGNOSTIC EXCEPTION DUMP ****\n"
         << error << endl
         << "***********************************" << endl;
  } catch(...) {
    cerr << "this sould not occur...\n";
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
