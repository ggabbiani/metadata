MetaData
--------

**Meta Data** is a hierarchical data recognition and management system written in C++.

Features
--------

- **Recognition system**
  The recognition system provided is hierarchically organised and configurable
  via json file. For every data object recognition cycle, a two step iteration is
  thrown from the root class to its children:

    1. **charset and mime recognition**: by using _magic library_, the actual charset and
    mime of the data is detected;

    2. **meta class detection**: data retrieved in the previous step is used as index for
    retrieving the proper meta class descriptor;

    3. **proxy creation** (optional): at this point _and if_ required a C++ proxy for the
    underlaying data is returned to the client. The proxy exposes to the caller all and only
    the methods and properties of the targeted data.

- **Data Management & Object-orientation**
Handling of data object recognized by the system, can be performed through the
use of the interface(s) implemented by the belonging class. Apart from the
basic interface offered by the root class, tha nature of the object is revealed
during run-time by the __Introspective__ rti functionalities. So, if a meta-data instance
object is recognized as a 'png' instance, the proxy will offer all the png
functionalities plus every methods and properties eventually inherited from the
parent class(es) (picture, regular-file etc.). Different paths of the
hierarchical tree could implement the same interface, for example a directory
meta-class instance, implements the Iterate interface, exactly as a tar
meta-class instance does.

- **Modularity & Extedibility**
System is implemented by shared-object libraries that obey to a common
interface design. In such a manner it's simple to implement a new meta-class
and add it to the recognition system. Once compiled successfully, copying the
code in the right place and adding the meta-class properties to the
configuration db it's enough for using the new class exactly as any other one.

Status
------

The classes implemented till now are the following:

* **Canonical**: base class
    * **System**: one of the recognized Unix file types
        * **Device**: Abstract class defining a system device, i.e. a block or character one.
            * **BlkDev**: block devices or randomly accessible device
            * **ChrDev**: character devices provide only a serial stream of input or output
        * **Directory**: directory is the most common Unix special file
        * **Fifo**: named pipes connecting the output of one process to the input of another
        * **RegularFile**: Abstract UNIX regular file class
            * **Tar**: USTAR archive format as defined in the POSIX (IEEE P1003.1) standard
            * **Text**: Text document data type, the inner format is UTF-8.
                * **Ascii**: Concrete ASCII class
                * **Utf8**: concrete meta-class for UTF-8 unicode text files
        * **Socket**: Abstract class defining a system socket.
            * **UnixSocket**: Concrete class defining a Unix domain socket.
        * **SymLink**: A symbolic link is a reference to another file.

Prerequisites:
--------------

- [Extensible Exception Library (EEL)](http://sourceforge.net/p/extensibleexceptionlibrary/) : simple framework for C++11 exception handling.
- [Introspective](http://sourceforge.net/projects/introspective/) : C++ message based, object oriented and dynamic inheritance framework.
- [Trivial Serialization Library (TSL)](http://sourceforge.net/p/trivialserializ/): C++11 serialization library.

Distro supported:
-----------------

- CentOS-7
- Debian-8
- Fedora-23
- Gentoo
- OpenSUSE-42.1
- Ubuntu-14
