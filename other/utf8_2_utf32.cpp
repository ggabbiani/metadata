#include <iostream>
#include <stdexcept>
#include <string>
#include <sstream>

using namespace std;

namespace other {

namespace utf32_test {

// value,mask pairs, index reflects the actual byte len - 1
static unsigned char mask[6][2] = {
  {0x00,0x80},
  {0xc0,0xe0},
  {0xe0,0xf0},
  {0xf0,0xf8},
  {0xf8,0xfc},
  {0xfc,0xfe}
};

static int cbc(unsigned char value) throw () {
  for( unsigned i=0; i<6; i++){
    if (((value & mask[i][1]) ^ mask[i][0]) == 0) {
      return i;
    }
  }
  return -1;
}

struct consumer {
  virtual void operator () (char32_t wchr) const throw () = 0;
};

struct null_consumer : consumer {
  virtual void operator () (char32_t wchr) const throw ();
};

void null_consumer::operator () (char32_t wchr) const throw () {
}

class appender : public consumer {
public:
  appender(wstring &destination) : _destination(destination) {}
  virtual void operator () (char32_t wchr) const throw ();
private:
  wstring &_destination;
};

void appender::operator () (char32_t wchr) const throw () {
  _destination += wchr;
}

struct error_handler {
  virtual bool operator () (const char* message) const throw (std::runtime_error) = 0;
};

struct ignore_error : public error_handler {
  bool operator () (const char* message) const throw (std::runtime_error);
};

bool ignore_error::operator () (const char* message) const throw (std::runtime_error) {
  return false;
}

struct rise_error : public error_handler {
  bool operator () (const char* message) const throw (std::runtime_error);
};

bool rise_error::operator () (const char* message) const throw (std::runtime_error) {
  throw runtime_error(std::string(__PRETTY_FUNCTION__) + ": " + message );
}

class decoder {
public:
  decoder(consumer& cons,error_handler& handler) : _consume(cons),_error(handler) {}
  bool operator () (istream&) const throw (std::runtime_error);
private:
  consumer&      _consume;
  error_handler& _error;
};

bool decoder::operator () (istream& in) const throw (std::runtime_error) {
  while(!in.eof()){
    unsigned char c = in.get();
    if (in.eof())
      break;
    if (!in.good())
      throw runtime_error("errno reading first char in sequence");
    int len = cbc(c);
    if (len==-1) {
      return _error("wrong byte sequence lenght");
    }
    char32_t wc = (c & ~mask[len][1]) << (len * 6);
    for( ;len ;len-- ){
      c = in.get();
      if (((c & 0xc0) ^ 0x80) != 0) {
        return _error("continuation pattern missing");
      }
      wc |= (c & ~0xc0) << ((len -1) * 6);
    }
    _consume(wc);
  }
  return true;
}

bool isUtf8(istream& in) {
  ignore_error eh;
  null_consumer cn;
  decoder decode(cn,eh);
  return decode(in);
}

void readUtf8(istream& in,wstring &destination) {
  rise_error eh;
  appender cn(destination);
  decoder decode(cn,eh);
  decode(in);
}

}

}

using namespace other::utf32_test;

int main(int argc,const char *argv[]) {
  // checking a stream
  {
    // good utf8 stream
    string u8s(u8"鵝滿是快烙滴好耳痛\U00002606");
    stringstream ss(u8s);
    cout << isUtf8(ss) << endl;
  }
  {
    // wrong utf8 stream 1
    string u8s((char*)U"鵝滿是快烙滴好耳痛\U00002606");
    stringstream ss(u8s);
    cout << isUtf8(ss) << endl;
  }
  {
    // wrong utf8 stream 2
    string u8s((char*)u"鵝滿是快烙滴好耳痛\U00002606");
    stringstream ss(u8s);
    cout << isUtf8(ss) << endl;
  }
  // reading of an UTF-8 stream into a UTF-32 string 
  {
    // good utf8 stream
    try {
      string u8s(u8"鵝滿是快烙滴好耳痛\U00002606");
      stringstream ss(u8s);
      wstring destination;
      readUtf8(ss,destination);
    } catch (const exception &error) {
      cerr << error.what() << endl;
    }
  }
  {
    // wrong utf8 stream 1 (actually a UTF-32)
    try {
      string u8s((char*)U"鵝滿是快烙滴好耳痛\U00002606");
      stringstream ss(u8s);
      wstring destination;
      readUtf8(ss,destination);
    } catch (const exception &error) {
      cerr << error.what() << endl;
    }
  }
  {
    // wrong utf8 stream 2 (actually a UTF-16)
    try {
      string u8s((char*)u"鵝滿是快烙滴好耳痛\U00002606");
      stringstream ss(u8s);
      wstring destination;
      readUtf8(ss,destination);
    } catch (const exception &error) {
      cerr << error.what() << endl;
    }
  }
}
