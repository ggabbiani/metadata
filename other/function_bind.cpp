/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <functional>
#include <string>

using namespace std;

namespace other {

using Function = function<double (int,const string&)>;

double flat_function(int i,const string &s) {
  return s.length() * i;
}

struct TestClass {
  string name;
  Function fn;
  double member_function(int i,const string &s) {
    return s.length() * i;
  }
};

}

int main(int argc,const char *argv[]) {
  using namespace std::placeholders;
  using namespace other;
  TestClass object{"gigio"};
  object.fn = bind(flat_function,3,_2);
  cout << object.fn(1,string("ciao")) << endl;
  object.fn = bind(&TestClass::member_function,object,_1,_2);
  cout << "object named " << object.name << ' ' << object.fn(2,string("ciao")) << endl;
}
