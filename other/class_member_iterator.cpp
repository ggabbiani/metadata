/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <eel/eel.h>
#include <functional>
#include <iostream>
#include <iomanip>
#include <typeindex>
#include <unordered_map>
#include <vector>

#include <boost/any.hpp>
#include <boost/lexical_cast.hpp>

#include <tsl/rtx.h>

using namespace std;
using namespace eel;

namespace rtx=tsl::rtx;
namespace a=eel::attribute;

namespace other {

namespace cmi {

class Property {
  friend class Iterator;
  friend class Iterable;
public:
  typedef std::function<void (Property &)> Initiator;
  typedef std::function< std::ostream &(std::ostream &,const boost::any &) > OutStreamer;
  template <typename T>
  Property(const std::string &name,const T &value) : _name(name), _value(value) {}
  const std::type_info &type() {return _value.type();}
  template <typename T>
  T &get() {return boost::any_cast<std::reference_wrapper<T>>(_value).get();}
  template <typename T>
  void set(const T &value) {get<T>()=value;}
  friend ostream& operator<<(ostream& out, const Property& property) {
    return property._dump(out,property._value);
  }
  const std::string &name() {return _name;}
private:
  boost::any _value;
  OutStreamer _dump;
  std::string _name;
  /** default constructor for iterators */
  Property() = default;
};

class Iterable {
  friend class Iterator;
public:
  virtual ~Iterable() = default;
protected:
  template <typename T> void define(const string &name,T &value);
private:
  void init(int position,Property &property) {_init[position](property);}
  std::size_t size() {return _init.size();}
  vector<Property::Initiator> _init;
};

template <typename T>
void Iterable::define(const string &name,T &value) {
  _init.push_back([this,name,&value] (Property &property) {
    property._name=name;
    property._value=std::ref(value);
    property._dump=[](ostream &out,const boost::any &v) -> ostream & {return out << boost::any_cast<std::reference_wrapper<T>>(v).get();};
  });
}

class Iterator : public std::iterator <std::input_iterator_tag,Property> {
public:
  Iterator() : _object(nullptr),position(-1) {}
  Iterator(Iterable &object);
  int position;
  /** indirection operator */
  reference operator * () {
    if (!referenceable()) {
      throw PreconditionViolation("iterator must be deferrable",EELC_DIAG);
    }
    _object->init(position,_property);
    return _property;
  }
  /** arrow operator */
  pointer operator->() {
    if (!referenceable()) {
      throw PreconditionViolation("iterator must be deferrable",EELC_DIAG);
    }
    _object->init(position,_property);
    return &_property;
  }
  Iterator &operator++() {
    if (position>-1) {
      ++position;
      if (!referenceable()) {
	position=-1;
      }
    }
    return *this;
  }
  bool operator == (const Iterator &other) {return position == other.position;}
  bool operator != (const Iterator &other) {return !operator==(other);}
private:
  bool referenceable() const {
    return position!=-1 && _object && _object->size()>position;
  }
  Property _property;
  Iterable *_object;
};

inline Iterator::Iterator(Iterable &object) : _object(&object),position(0) {
}

struct Class : public virtual Iterable {
  Class(int p1,double p2,const string &p3);
  int anInt;
  double aDouble;
  string aString;
};

Class::Class(int p1,double p2,const string &p3) : anInt(p1),aDouble(p2),aString(p3) {
  define("Class::anInt",anInt);
  define("Class::aDouble",aDouble);
  define("Class::aString",aString);
}

}

}

using namespace other::cmi;

int main(int argc,const char *argv[]) {
  try {
    Class object(1,2.2,"I'm a string");
    Iterator i(object);
    Iterator end;
    // first in sequence is an int
    cout << '\'' << i->name() << "' " << rtx::demangle(i->type()) << '=' << *i << endl;
    i->set(100);
    cout << '\'' << i->name() << "' " << rtx::demangle(i->type()) << '=' << *i << endl;
    assert(i->get<int>()==object.anInt);
    // second in sequence is a double
    ++i;
    cout << '\'' << i->name() << "' " << rtx::demangle(i->type()) << '=' << *i << endl;
    i->set(1.23);
    cout << '\'' << i->name() << "' " << rtx::demangle(i->type()) << '=' << *i << endl;
    assert(i->get<double>()==object.aDouble);
    // third in sequence is a string
    ++i;
    cout << '\'' << i->name() << "' " << rtx::demangle(i->type()) << '=' << *i << endl;
    i->set<string>("another string");
    cout << '\'' << i->name() << "' " << rtx::demangle(i->type()) << '=' << *i << endl;
    assert(i->get<string>()==object.aString);
    for(Iterator i(object);i!=Iterator();++i) {
      cout << '\'' << i->name() << "' " << rtx::demangle(i->type()) << '=' << *i << endl;
    }
  } catch (const eel::Exception &error) {
    cerr << error.what() << endl
         << error << endl;
  } catch (const exception &error) {
    cerr << error.what() << endl;
  }
}
