/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <stdio.h>
#include <magic.h>
#include <eel/eel.h>

using namespace std;

namespace other {

struct Magic {
  Magic(int flags=MAGIC_MIME) {
    cookie = magic_open(flags);
    if (!cookie) {
      throw eel::RuntimeError(eel::attribute::what(magic_error(cookie)),EELC_DIAG);
    }
    if (magic_load(cookie,nullptr) != 0) {
      throw eel::RuntimeError(eel::attribute::what(magic_error(cookie)),EELC_DIAG);
    }
  }
  ~Magic() {
    magic_close(cookie);
  }
  const char *describe(const char *file) {
    return magic_file(cookie,file);
  }
  magic_t cookie = nullptr;
};

}

int main(int argc,const char *argv[]) {
  using namespace other;
  if (argc < 2) {
    cerr << "missing args" << endl;
    exit(1);
  }
  const char *actual_file = argv[1];
  Magic magic;
  if (const char *desc = magic.describe(actual_file)) {
    cout << desc << endl;
  }
  return 0;
}
