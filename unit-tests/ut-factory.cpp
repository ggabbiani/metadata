/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#define BOOST_TEST_MODULE factory_test_module
#include <boost/test/unit_test.hpp>

#include <eel/eel.h>
#include <introspective/system.h>

#include "md/system.h"
#include "ut_config.h"

using namespace boost::unit_test;
using namespace eel;
using namespace md;
using namespace std;

namespace ut {

namespace factory {

BOOST_AUTO_TEST_CASE( factory_instance_test ) {
  try {
    Factory& factory = Factory::configure(TU_CONFIG);
    introspective::Class *cls        = introspective::system [ introspective::msg::GetClass("system") ];
    Descriptor           *descriptor = factory.find("system");
    BOOST_REQUIRE(cls == descriptor);
  } catch (const eel::Exception &error) {
    cerr << "*** DIAGNOSTIC DUMP *** \n" 
         << error << endl;
    throw;
  }
}

}

}
