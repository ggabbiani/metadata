/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#define BOOST_TEST_MODULE generic test case
#include <boost/test/unit_test.hpp>
#include <boost/test/output_test_stream.hpp>

#include <boost/spirit/include/qi.hpp>
#include <introspective/proxy.h>
#include <tsl/rtx.h>

#include "debug/trace.h"

#include "md/ascii.h"
#include "md/blockdevice.h"
#include "md/characterdevice.h"
#include "md/directory.h"
#include "md/fifo.h"
#include "md/msgmanip.h"
#include "md/system.h"
#include "md/symlink.h"
#include "md/tar.h"
#include "md/utf8.h"

#include "ut_config.h"

using namespace md;
using namespace std;

namespace iproxy=introspective::proxy;
namespace fs    =boost::filesystem;
namespace rtx   =tsl::rtx;

namespace ut {

namespace describe {

// retrieve the Factory instance
Factory& factory = Factory::configure(TU_CONFIG);

BOOST_AUTO_TEST_CASE( ascii_data_test ) {
  try {
    Descriptor &type(factory.describe(TU_DATA"/ascii.txt"));
    BOOST_REQUIRE(type.instanceId == typeid(md::Ascii));
    BOOST_REQUIRE(!type.abstract());
//     cout << *(introspective::Class*)type << endl;
    BOOST_TEST_MESSAGE(type.name() << "\t\t\tok");
  } catch (const eel::Exception &error) {
    cerr << error << endl;
    throw;
  }
}

BOOST_AUTO_TEST_CASE(ascii_visitor_test) {
  using namespace md::text;
  try {
    iproxy::UniquePtr< Ascii > object(factory.create< Ascii >(TU_DATA"/ascii.txt"));
    BOOST_REQUIRE(object);
    Iterate::Visitor lcounter = [] (const Iterate::Iterator &line,void *result) {
      ++(*(int *)result);
      return true;
    };
    int lines = 0;
    object [ Iterate(lcounter,&lines) ];
    BOOST_REQUIRE(lines == 3);
    BOOST_TEST_MESSAGE(object->parent().name() << " iterate \t\tok");
  } catch (const eel::Exception &error) {
    cerr << error << endl;
    throw;
  }
}

/*
 * desc
 */
BOOST_AUTO_TEST_CASE( text_read_test ) {
  using boost::test_tools::output_test_stream;
  using namespace md::text;
  output_test_stream out(TU_DATA"/ascii.txt",true);
  try {
    iproxy::UniquePtr< Text > object(factory.create< Text >(TU_DATA"/ascii.txt"));
    BOOST_REQUIRE(object);
    out << object [ introspective::msg::GetAs< string >(text::property::CONTENT) ];
  } catch (const eel::Exception &error) {
    cerr << error << endl;
    throw;
  }
}


BOOST_AUTO_TEST_CASE( fifo_data_test ) {
  const Descriptor &type = factory.describe(TU_DATA"/fifo");
  BOOST_REQUIRE(type.instanceId==typeid(md::Fifo));
  BOOST_REQUIRE(!type.abstract());
  BOOST_TEST_MESSAGE(type.name() << "\t\t\tok");
}

BOOST_AUTO_TEST_CASE( directory_data_test ) {
  const Descriptor &type = factory.describe(TU_DATA);
  BOOST_REQUIRE(type.instanceId==typeid(md::Directory));
  BOOST_REQUIRE(!type.abstract());
  BOOST_TEST_MESSAGE(type.name() << "\t\tok");
}

BOOST_AUTO_TEST_CASE(directory_visitor_test) {
  using namespace md::directory;
  try {
    iproxy::UniquePtr< Directory > object(factory.create< Directory >(TU_DATA));
    BOOST_REQUIRE(object);
    //     cout << object << endl;
    vector<Iterate::Entry> entries;
    Iterate::Visitor visitor = [] (const Iterate::Iterator &i,void *data) -> bool {
      TR_FUNC;
      vector<Iterate::Entry> *entries = (vector<Iterate::Entry> *)data;
      entries->push_back(*i);
      return true;
    };
    object [ Iterate(visitor,&entries) ];
    for(auto i : entries) {
      //       cout << i << endl;
    }
    BOOST_TEST_MESSAGE(object->parent().name() << " iterate\tok");
  } catch (const eel::Exception &error) {
    cerr << error << endl;
    throw;
  }
}

BOOST_AUTO_TEST_CASE( directory_enumeration_test ) {
  using namespace md::directory;
  using md::msg::Enumerate;
  try {
    iproxy::UniquePtr< Directory > object(factory.create< Directory >(TU_DATA));
    BOOST_REQUIRE(object);
    //     cout << object << endl;
    std::remove_reference< Enumerate::Result >::type entries;
    object [ Enumerate(entries) ];
    for(auto i = entries.begin();i != entries.end(); ++i) {
      Canonical *object = i->get();
      cout << object->name() << " type '" << object->parent().name() << '\'' << endl;
    }
    BOOST_TEST_MESSAGE(object->parent().name() << " enumeration\tok");
  } catch (const eel::Exception &error) {
    cerr << error << endl;
    throw;
  }
}

BOOST_AUTO_TEST_CASE( symlink_data_test ) {
  const Descriptor &type = factory.describe(TU_DATA"/ascii.txt.link");
  BOOST_REQUIRE(type.instanceId == typeid(md::SymLink));
  BOOST_REQUIRE(!type.abstract());
  BOOST_TEST_MESSAGE(type.name() << "\t\t\tok");
}

BOOST_AUTO_TEST_CASE( tar_data_test ) {
  const Descriptor &type = factory.describe(TU_DATA"/archive.tar");
  BOOST_REQUIRE(type.instanceId == typeid(md::Tar));
  BOOST_REQUIRE(!type.abstract());
  BOOST_TEST_MESSAGE(type.name() << "\t\t\tok");
}

BOOST_AUTO_TEST_CASE( tar_iterate_test ) {
  using namespace md::tar;
  try {
    iproxy::UniquePtr< Object > object(new Tar(TU_DATA"/archive.tar"));
    BOOST_REQUIRE(object);
//     cout << object << endl;
    vector<string> entries;
    Iterate::Visitor collect = [] (const Iterate::Iterator &i,void *data) -> bool {
      TR_FUNC;
      vector<string> *entries = (vector<string>*)data;
      entries->push_back(i->pathName);
      return true;
    };
    (*object) [ tar::Iterate( collect,&entries) ];
    for(auto i : entries) {
//       cout << i << endl;
    }
    BOOST_TEST_MESSAGE(object->parent().name() << " iterate\t\tok");
  } catch (const eel::Exception &error) {
    cerr << error << endl;
    throw;
  }
}

BOOST_AUTO_TEST_CASE( utf_8_data_test ) {
  const Descriptor &type = factory.describe(TU_DATA"/UTF-8");
  BOOST_REQUIRE(type.instanceId == typeid(md::Utf8));
  BOOST_REQUIRE(!type.abstract());
  BOOST_TEST_MESSAGE(type.name() << "\t\t\tok");
}

// BOOST_AUTO_TEST_CASE( blockdevice_data_test ) {
//   using namespace md::blkdev;
//   using namespace introspective::proxy;
//   namespace qi = boost::spirit::qi;
//   try {
//     Pointer< unique_ptr< Ascii > > partitions(new Ascii("/proc/partitions"));
//     text::Iterate::Visitor validator = [] (const text::Iterate::Iterator &i,void *data) {
//       string device;
//       bool r = qi::phrase_parse(i->begin(),i->end(),qi::omit[qi::int_ >> qi::int_ >> qi::int_] >> *qi::char_,boost::spirit::ascii::space,device);
//       fs::path p("/dev/"+device);
//       BOOST_REQUIRE(factory.describe<Device>(p)->instanceId == typeid(md::BlkDev));
//       return true;
//     };
//     // an Iterate message has a skip visitor as default
//     text::Iterate iterate;
//     // so skip first two lines
//     partitions [ iterate << md::count(2) ];
//     // validate all the other ones
//     redo [ iterate << md::visitor< text::Iterate >(validator) << md::count(-1) ];
//     BOOST_TEST_MESSAGE("blkdev\t\t\tok");
//   } catch (const eel::Exception &error) {
//     cerr << error.what() << endl
//          << "*** DIAGNOSTIC ***" << endl
//          << error << endl;
//     throw;
//   }
// }

BOOST_AUTO_TEST_CASE( chardevice_data_test ) {
  const Descriptor &type = factory.describe("/dev/null","device");
  BOOST_REQUIRE(type.instanceId == typeid(md::ChrDev));
  BOOST_REQUIRE(!type.abstract());
  BOOST_TEST_MESSAGE(type.name() << "\t\t\tok");
}

}

}
