/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#define BOOST_TEST_MODULE serialization test case
#include <boost/test/unit_test.hpp>

// #include <iostream>
// #include <memory>
// #include <stdexcept>
// #include <stdlib.h>
// #include <unistd.h>
// 
// #include <boost/spirit/include/qi.hpp>

#include <tsl/json_archive.h>
#include <tsl/xml_archive.h>

#include "md/ascii.h"
// #include "md_blockdevice.h"
// #include "md_characterdevice.h"
// #include "md_directory.h"
#include "md/fifo.h"
// #include "md_system.h"
// #include "md_symlink.h"
// #include "md_tar.h"
// #include "md_utf_8.h"
// #include "md_utf_32.h"

#include "ut_config.h"

using namespace boost::unit_test;
using namespace md;
using namespace std;

namespace ut {

namespace serialization {

Factory& factory = Factory::instance();

BOOST_AUTO_TEST_CASE( ascii_data_test ) {
  unique_ptr<Ascii> obj(new Ascii(TU_DATA"/ascii.txt"));
  tsl::OJsonArchive archive(cout);
  archive & *obj;
}

BOOST_AUTO_TEST_CASE( fifo_data_test ) {
  unique_ptr<Fifo> obj(new Fifo(TU_DATA"/fifo"));
  tsl::OJsonArchive archive(cout);
  archive & *obj;
}
// 
// BOOST_AUTO_TEST_CASE( directory_data_test ) {
//   const Descriptor* descriptor = factory.describe(TU_DATA"/directory");
//   BOOST_REQUIRE(descriptor);
//   BOOST_REQUIRE(descriptor->name() == directory::ID);
//   BOOST_TEST_MESSAGE(directory::ID << "	ok");
// }
// 
// BOOST_AUTO_TEST_CASE( directory_iterator_data_test ) {
//   boost::scoped_ptr<directory::Object> obj(factory.create<directory::Object>(TU_DATA));
//   BOOST_REQUIRE(obj);
//   boost::scoped_ptr<interface::Container::NameIterator> iter(obj->newNameIterator());
//   BOOST_REQUIRE(iter);
//   while (iter->hasMoreElements()) {
//     std::string& current = iter->next();
//     BOOST_TEST_MESSAGE("current:'" << current << '\'');
//   }
//   BOOST_TEST_MESSAGE(directory::ID << " iter	ok");
// }
// 
// BOOST_AUTO_TEST_CASE( tar_data_test ) {
//   const Descriptor* descriptor = factory.describe(TU_DATA"/archive.tar");
//   BOOST_REQUIRE(descriptor);
//   BOOST_REQUIRE(descriptor->name() == tar::ID);
//   BOOST_TEST_MESSAGE(tar::ID << "		ok");
// }
// 
// BOOST_AUTO_TEST_CASE( tar_iterator_data_test ) {
//   boost::scoped_ptr<tar::Object> obj(factory.create<tar::Object>(TU_DATA"/archive.tar"));
//   BOOST_REQUIRE(obj);
//   boost::scoped_ptr<interface::Container::NameIterator> iter(obj->newNameIterator());
//   BOOST_REQUIRE(iter);
//   while (iter->hasMoreElements()) {
//     std::string& current = iter->next();
//     BOOST_REQUIRE(current == "ascii.txt");
//   }
//   BOOST_TEST_MESSAGE(tar::ID << " iter	ok");
// }
// 
// BOOST_AUTO_TEST_CASE( utf_8_data_test ) {
//   const Descriptor* descriptor = factory.describe(TU_DATA"/UTF-8");
//   BOOST_REQUIRE(descriptor);
//   BOOST_REQUIRE(descriptor->name() == utf_8::ID);
//   BOOST_TEST_MESSAGE(utf_8::ID << "		ok");
// }
// 
// BOOST_AUTO_TEST_CASE( blockdevice_data_test ) {
//   namespace qi = boost::spirit::qi;
//   try {
//     shared_ptr<ascii::Object> partitions(factory.create<ascii::Object>("/proc/partitions"));
//     LineIterator line(*partitions);
//     ++line;++line;
//     for(;line!=LineIterator();++line) {
//       wstring device;
//       bool r = qi::phrase_parse(line->begin(),line->end(),qi::omit[qi::int_ >> qi::int_ >> qi::int_] >> *qi::char_,boost::spirit::ascii::space,device);
//       boost::filesystem::path p(L"/dev/"+device);
//       BOOST_REQUIRE(factory.describe(p)->is(blkdev::ID));
//     }
//   } catch (const eel::Exception &error) {
//     cerr << error.what() << endl
//          << "*** DIAGNOSTIC ***" << endl
//          << error.dump() << endl;
//     throw;
//   }
// }
// 
// BOOST_AUTO_TEST_CASE( chardevice_data_test ) {
//   const Descriptor* descriptor = factory.describe("/dev/null");
//   BOOST_REQUIRE(descriptor);
//   BOOST_REQUIRE(descriptor->name() == chrdev::ID);
//   BOOST_TEST_MESSAGE(chrdev::ID << "		ok");
// }
// 
// BOOST_AUTO_TEST_CASE( symlink_data_test ) {
//   const Descriptor* descriptor = factory.describe(TU_DATA"/ascii.txt.link");
//   BOOST_REQUIRE(descriptor);
//   BOOST_REQUIRE(descriptor->name() == symlink::ID);
//   BOOST_TEST_MESSAGE(symlink::ID << "		ok");
// }
// 
// BOOST_AUTO_TEST_CASE( descendent_test ) {
//   const Descriptor* descriptor = factory.describe(TU_DATA"/ascii.txt");
//   BOOST_REQUIRE(descriptor);
//   BOOST_REQUIRE(descriptor->is(text::ID));
//   BOOST_TEST_MESSAGE(descriptor->name() << " is " << text::ID);
// }

}

}
