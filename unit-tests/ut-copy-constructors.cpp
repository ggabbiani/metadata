/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#define BOOST_TEST_MODULE generic test case
#include <boost/test/unit_test.hpp>

#include "ut_config.h"
#include "md/ascii.h"

// #include <tsl/iarchive.h>

using namespace md;
using namespace std;

namespace ut {

namespace copy_constructors {

// retrieve the Factory instance
// Factory &factory = Factory::instance();

BOOST_AUTO_TEST_CASE(ascii_copy_constructor_test) {
  try {
    Factory &factory = Factory::instance();
     unique_ptr<Ascii> original(new Ascii(TU_DATA"/ascii.txt"));
    Ascii copy(*original);
  } catch (const tsl::ParserError &error) {
    cerr << error << endl
         << tsl::parser::Helper(error) << endl;
    BOOST_FAIL(error.what());
  } catch (const eel::Exception &error) {
    cerr << error << endl;
    BOOST_FAIL(error.what());
  }
}

}

}
