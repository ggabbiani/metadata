/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MD_SYMLINK_H
#define MD_SYMLINK_H

#include <md/system.h>

namespace md {

/** concrete meta-class for symbolic links */
namespace symlink {

/** namespace for symbolic link properties */
namespace property {

/**
 * LNK_TARGET (std::string) is the actual link target.
 */
constexpr const char *TARGET = "LNK_TARGET";

}

/**
 * A symbolic link is a reference to another file.
 *
 * NEW PROPRTIES
 *
 * - symlink::property::TARGET
 */
class Object : public System {
  friend bool initialize(tsl::rtx::ClassInfo::Registry *registry,void * descriptor);
public:
  /** Allocator's file constructor */
  explicit Object(const Path &path);
protected:
  /** FIXME copy constructor to be implemented */
  explicit Object(const Object &other) = delete;
  /** derived meta class constructor */
  Object(Descriptor *descriptor,const Path &path) : System(descriptor,path) {
  }
};

}

using SymLink = symlink::Object;

}

#endif // MD_SYMLINK_H
