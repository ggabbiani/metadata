/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

// #define DOTRACE
#include "debug/trace.h"

#include <eel/eel.h>

#include "md/messages.h"
#include "md/properties.h"
#include "md/symlink.h"

using namespace eel;
using namespace std;
using namespace tsl::rtx;

namespace {

md::Descriptor* DESCRIPTOR = nullptr;

}

namespace md {

namespace symlink {

Object::Object(const Path &path) : System(DESCRIPTOR,path) {
  char buffer[256];
  /* Attempt to read the target of the symbolic link. */
  int len = readlink(path.c_str(),buffer,sizeof(buffer));
  if (len == -1) {
    throw Errno(EELC_DIAG);
  }
  /* NUL-terminate the target path. */
  buffer[len] = '\0';
  string target = buffer;
  define(property::TARGET,target);
  define(md::property::DESCRIPTION,string("symbolic link to '") + target + '\'');
}

}

}

extern "C" bool initialize(ClassInfo::Registry *,void * descriptor) {
  using namespace md;
  return Descriptor::initConcrete<SymLink,Descriptor::MIME_KEY>(descriptor,DESCRIPTOR);
}
