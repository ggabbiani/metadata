/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#define DOTRACE
#include "debug/trace.h"
#include "md/executable.h"

using namespace std;

namespace {

md::Descriptor* DESCRIPTOR = nullptr;

void cm_Execute(introspective::Object &receiver,introspective::Message &message) {
  TR_FUNC;
  TR_MSG("&recipient " << message.recipient());
  TR_MSG("&receiver " << &receiver << " (" << receiver.name() << ")");
  TR_MSG("DESCRIPTOR " << DESCRIPTOR << " (" << DESCRIPTOR->name() << ")");
  assert(&receiver == DESCRIPTOR);
  assert(message.recipient());
  using md::executable::Execute;
  using md::Canonical;
  Execute   &msg       = dynamic_cast< Execute& >(message);
  Canonical &recipient = dynamic_cast< Canonical& >(*msg.recipient());
  string cmdline = recipient.name();
  if (not msg.args.empty()) {
    cmdline += ' ' + msg.args;
  }
  TR_MSG(cmdline);
  msg.result = system(cmdline.c_str());
  TR_MSG(msg.result);
}

}

namespace md {

namespace executable {

Object::Object(const Path &path) : Object(DESCRIPTOR,path) {
}

Object::Object(Descriptor *descriptor,const Path &path) : ELF(descriptor,path) {
}

}

}

extern "C" bool initialize(tsl::rtx::ClassInfo::Registry *registry,void *ioarea) {
  TR_FUNC;
  using namespace md;
  using std::placeholders::_2;
  TR_MSG("DESCRIPTOR " << DESCRIPTOR);
  if (Descriptor::initConcrete<Executable,Descriptor::MIME_KEY>(ioarea,DESCRIPTOR)) {
    TR_MSG("DESCRIPTOR " << DESCRIPTOR);
    DESCRIPTOR->bind< executable::Execute >(std::bind(&cm_Execute,*(introspective::Object*)DESCRIPTOR,_2));
    return true;
  }
  return false;
}
