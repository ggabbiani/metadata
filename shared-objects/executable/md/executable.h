/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <md/elf.h>

namespace md {

/** namespace for ELF executables */
namespace executable {

/**
 * Concrete class for ELF executables.
 */
class Object : public ELF {
public:
  explicit Object(const Path &path);
protected:
  /** derived meta class constructor */
  Object(Descriptor *descriptor,const Path &path);
private:
  /** manages a md::executable::Execute message */
  void mth_Execute(introspective::Message &msg);
};

/**
 * Execute an executable object.
 *
 * Signature: [ Execute ] -> int
 *
 */
struct Execute : public introspective::msg::Adaptor< Execute,int > {
  Execute();
  Execute(const char *parms) : args(parms) {
  }
  Execute(const std::string &parms) : args(parms) {
  }
  std::string args;
};

}

using Executable = executable::Object;

}
