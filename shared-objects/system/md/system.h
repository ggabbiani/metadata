/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MD_SYSTEM_H
#define MD_SYSTEM_H

#include <md/md.h>

#include <boost/filesystem.hpp>

namespace md {

/** abstract meta-class for system objects. */
namespace system {

/** namespace for system properties */
namespace property {

/**
 * SYS_TYPE (int) system types recognized by system meta class
 */
constexpr const char *TYPE = "SYS_TYPE";

}

/** namespace for system operations */
namespace msg {

/**
  * Abstraction for the POSIX open() function. The inner mechanism is
  * implementation dependent.
  *
  * Signature: [ Open ] -> void*
  */
struct Open : public introspective::msg::Adaptor< Open,void* > {

};

/**
  * Abstraction for the POSIX close() function. The inner mechanism is
  * implementation dependent.
  *
  * Signature: [ Close ] -> void*
  */
struct Close : public introspective::msg::Adaptor< Close,void* > {

};

/**
  * Abstraction for the POSIX read() function. The inner mechanism is
  * implementation dependent.
  *
  * Signature: [ Read ] -> void*
  */
struct Read : public introspective::msg::Adaptor< Read,void* > {

};

/**
  * Abstraction for the POSIX write() function. The inner mechanism is
  * implementation dependent.
  *
  * Signature: [ Write ] -> void*
  */
struct Write : public introspective::msg::Adaptor< Write,void* > {

};

/**
  * The receiver initializes the recipient system properties.
  */
struct Stat : public introspective::msg::Adaptor< Stat, boost::filesystem::file_status > {
};

}

/**
 * one of the recognized Unix file types.
 *
 * NEW PROPERTIES
 *
 * - system::property::TYPE
 */
class Object : public Canonical {
protected:
  virtual void serialize(tsl::OArchive &out) const;
  virtual void deserialize(tsl::IArchive &in);
  /** derived meta class constructor */
  Object(Descriptor *descriptor,const Path &p);
};

}

using System = system::Object;

}

#endif
