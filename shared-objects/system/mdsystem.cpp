/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <fcntl.h>

#include <tsl/iarchive.h>
#include <tsl/oarchive.h>
#include <tsl/rtx.h>

#include "md/md.h"
#include "md/messages.h"
#include "md/system.h"

#ifndef NDEBUG
#include "debug/trace.h"
#endif

using namespace std;
namespace fs = boost::filesystem;

namespace {

md::Descriptor *DESCRIPTOR = nullptr;

}

namespace md {

namespace system {

static void stat(introspective::Object&,introspective::Message &message) {
  msg::Stat &msg = dynamic_cast< msg::Stat& >(message);
  md::Canonical &recipient = dynamic_cast< md::Canonical& >(*msg.recipient());
  recipient.define(property::TYPE,fs::symlink_status(recipient.name()).type());
}

//**** Object *****************************************************************

Object::Object(md::Descriptor *descriptor, const fs::path &p) : Canonical(descriptor,p) {
//   define(property::TYPE,fs::symlink_status(p).type());
  (*this) [ msg::Stat() ];
}

void system::Object::serialize(tsl::OArchive &out) const {
  Canonical::serialize(out);
}

void system::Object::deserialize(tsl::IArchive &in) {
  Canonical::deserialize(in);
}

}

}

extern "C" bool initialize(tsl::rtx::ClassInfo::Registry *registry,void * io) {
  using std::placeholders::_1;
  using std::placeholders::_2;
  using namespace md;
  if (Descriptor::initAbstract<System>(io,DESCRIPTOR)) {
    DESCRIPTOR->bind< system::msg::Stat >(std::bind(&system::stat,_1,_2));
    return true;
  } else {
    return false;
  }
}
