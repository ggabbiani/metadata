cmake_minimum_required(VERSION 2.8)
#cmake_policy(SET CMP0048 NEW)
project(mdblockdevice )
enable_testing()

set(PARENT "mddevice")
set(CMAKE_INCLUDE_CURRENT_DIR "ON")

add_library(${PROJECT_NAME} SHARED ${PROJECT_NAME}.cpp)
set_target_properties(${PROJECT_NAME} PROPERTIES VERSION "${PROJECT_VERSION}")
target_link_libraries(${PROJECT_NAME}
${PARENT}
)

install(TARGETS ${PROJECT_NAME} LIBRARY DESTINATION "${MD_LIB_INSTALL_DIR}")
install(DIRECTORY md DESTINATION include)

if ("${MD_SOPATH}" STREQUAL "")
  set(MD_SOPATH "${CMAKE_CURRENT_BINARY_DIR}" CACHE STRING "TEST PATH" FORCE)
else ("${MD_SOPATH}" STREQUAL "")
  set(MD_SOPATH "${MD_SOPATH}:${CMAKE_CURRENT_BINARY_DIR}" CACHE STRING "TEST PATH" FORCE)
endif ("${MD_SOPATH}" STREQUAL "")
