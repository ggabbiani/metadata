/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MD_UNIX_SOCKET_H
#define MD_UNIX_SOCKET_H

#include <md/socket.h>

namespace md {

/** concrete meta-class for unix domain socket */
namespace unix_socket {

/**
 * Concrete class defining a Unix domain socket.
 */
class Object : public Socket {
public:
  /** Allocator's file constructor */
  explicit Object(const Path &path);
protected:
  /** derived meta class constructor */
  Object(Descriptor *descriptor,const Path &path) : Socket(descriptor,path) {}
};

}

using UnixSocket = unix_socket::Object;

}

#endif
