/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "md/characterdevice.h"
#include "md/properties.h"

using namespace std;

namespace {

md::Descriptor* DESCRIPTOR = 0;

}

namespace md {

namespace chrdev {

Object::Object(Descriptor *descriptor,const Path &path) : Device(descriptor,path) {
  int major = (*this) [ introspective::msg::GetAs<int>(device::property::MAJOR) ];
  int minor = (*this) [ introspective::msg::GetAs<int>(device::property::MINOR) ];
  define(property::DESCRIPTION, "character special (" + boost::lexical_cast<string>(major) + '/' + boost::lexical_cast<string>(minor) + ')' );
}

Object::Object(const Path &path): ChrDev(DESCRIPTOR,path) {
}

}

}

extern "C" bool initialize(tsl::rtx::ClassInfo::Registry *,void *descriptor) {
  using namespace md;
  return Descriptor::initConcrete<ChrDev,Descriptor::MIME_KEY>(descriptor,DESCRIPTOR);
}
