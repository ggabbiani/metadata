/***************************************************************************
 *   Copyright (C) 2005 by Giampiero Gabbiani                              *
 *   giampiero_gabbiani@tin.it                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef MD_TAR_STREAM_H
#define MD_TAR_STREAM_H

#include <stdexcept>
#include <fstream>

#include "md_system.h"

namespace md {

class Object;

namespace tar {

/** convenience class performing stream-like write operation(s) upon tar object. */
class ostream : protected std::ofstream {
public:
  explicit ostream(const char * filename,openmode mode = out | trunc) throw(std::runtime_error);
  void close();
  ostream& flush() {std::ofstream::flush(); return *this;}
  ostream& operator << (system::Object &object) throw(std::runtime_error);
};

}

}

}

#endif // MD_TAR_STREAM_H
