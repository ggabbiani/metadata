/***************************************************************************
 *   Copyright (C) 2005 by Giampiero Gabbiani                              *
 *   giampiero_gabbiani@tin.it                                             *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <boost/filesystem/operations.hpp>
#include <boost/scoped_ptr.hpp>
#include <string>
//#include <cpputils/trace.h>

#include "md_directory.h"
// #include "md_private.h"
#include "md_tar.h"
#include "md_tarstream.h"

using namespace std;

namespace meta {

namespace data {

namespace tar {

extern Descriptor* DESCRIPTOR;

static const char* fnameChk(const char* filename)  throw(runtime_error) {
  //TR_FUNC;
  if (!DESCRIPTOR->match(filename))
    MD_THROW(runtime_error,"'"+filename+"' is not suitable for tar name.");
  return filename;
}

ostream::ostream(const char * filename,openmode mode) throw(std::runtime_error) : std::ofstream(fnameChk(filename),mode) {
  //TR_FUNC;
}

ostream& ostream::operator << (meta::data::system::Object& object) throw(std::runtime_error) {
  //TR_FUNC;
  interface::Streamable* streamable = dynamic_cast<interface::Streamable*>(&object);
  if (streamable) {
    Header header(&object);
    write((const char*)&header,sizeof(Header));
    streamable->write(*this);
    int remaining = ((object.size() + 511) & ~511) - object.size();
    //TR_MSG("system size "<<object.size());
    //TR_MSG("round up " << ((object.size() + 511) & ~511));
    //TR_MSG("rem. " << remaining);
    for( int i=0;i<remaining ;i++ )
      *this << '\0';
  } else {
    directory::Object* directory = dynamic_cast<directory::Object*>(&object);
    if (!directory)
      MD_THROW(runtime_error,object.name() + ": cannot handle " + object.descriptor()->name() + " instances.");
    //TR_MSG("directory found");
    Header header(&object);
    write((const char*)&header,sizeof(Header));
    // first write of 512 null bytes for data...
    Header null;
    write((const char*)&null,sizeof(null));
  
    struct DemoFilter : public functional::Filter {
      virtual bool operator () (const std::string& value) {return value!="." && value!="..";}
      /** @return true if filter is assign()-ed, false otherwise. */
      virtual bool empty() const {return false;}
    };
    DemoFilter filter;
    typedef std::vector<std::string> StringVector;
    StringVector dirContents;
    directory->list(dirContents,&filter);
    sort(dirContents.begin(),dirContents.end());
    for(StringVector::size_type i=0;i<dirContents.size();i++) {
      boost::filesystem::path elementName = boost::filesystem::path(directory->name()) / boost::filesystem::path(dirContents[i]);
      //TR_MSG("including "<<i<<"-element '"<<elementName.string()<<'\'');
      boost::scoped_ptr<system::Object> dirObject(meta::data::Factory::instance().create(elementName.string().c_str()));
      if (!dirObject.get()) 
        MD_THROW(runtime_error,"No descriptor found for '"+elementName.string()+'\'');
      *this << *dirObject.get();
    }
  }
  return *this;
}

void ostream::close() {
  //TR_FUNC;
  Header null;
  write((const char*)&null,sizeof(null));
  write((const char*)&null,sizeof(null));
}

}

}

}
