/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <archive.h>
#include <archive_entry.h>
#include <eel/eel.h>

#include "debug/trace.h"
#include "archive++/tar.h"
#include "md/messages.h"
#include "md/tar.h"

using namespace std;
using namespace eel;
using namespace tsl::rtx;

using md::Factory;

namespace attr          = eel::attribute;
namespace fs            = boost::filesystem;
namespace regular       = md::regular_file;
namespace sys		= md::system;

namespace {

md::Descriptor* DESCRIPTOR = nullptr;

}


namespace md {

namespace tar {

struct Callback : public arc::tar::Reader::Callback {
  int open(struct archive *)                         override;
  bool isOpen(struct archive *)                      override;
  ssize_t read(struct archive *,const void* &buffer) override;
  int close(struct archive *)                        override;
  constexpr static int bsize = 10240;
  int fd = -1;
  char buffer[bsize];
};

int Callback::open(struct archive *a) {
  fd = ::open(fname.c_str(), O_RDONLY);
  return (fd >= 0 ? ARCHIVE_OK : ARCHIVE_FATAL);
}
ssize_t Callback::read(struct archive *a,const void* &buffer) {
  buffer = this->buffer;
  return (::read(this->fd, this->buffer, bsize));
}
int Callback::close(struct archive *a) {
  if (this->fd > 0)
    ::close(this->fd);
  return (ARCHIVE_OK);
}

bool Callback::isOpen(struct archive *a) {
  return this->fd >= 0;
}

//**** Object *****************************************************************

Object::Object(const Path &path): Object(DESCRIPTOR, path) {
}

Object::Object(Descriptor *descriptor,const Path &path) : RegularFile(descriptor,path) {
  using std::placeholders::_2;
  bind< msg::Enumerate >(std::bind(&Object::mth_Enumerate,this,_2));
  bind< msg::Iterate   >(std::bind(&Object::mth_Iterate,  this,_2));
}

void Object::mth_Enumerate(introspective::Message &message) {
  using md::msg::Enumerate;
  Enumerate         &msg    = dynamic_cast< Enumerate& >(message);
  Enumerate::Result  result = *msg.result;
  Factory           &factory = Factory::singleton();
  // to this visitor a nullptr is passed as data, result are accessed directly
  // through lambda enclosure.
  // FIXME this lambda is actually a do-nothing...
  Iterate::Visitor visitor = [ &result,&factory ] (const Iterate::Iterator &i,void *data) -> bool {
    TR_FUNC;
//     // a directory iterator hods a boost::filesystem::directory_entry
//     auto path = i->path();
//     // ask factory to create an object from path
//     Canonical *optr = factory.create(path);
//     // assign the object to a std::unique_ptr
//     introspective::proxy::UniquePtr< Canonical > object(optr);
//     // push back the result
//     result.push_back(std::move(object));
    return true;
  };
  (*this) [ tar::Iterate(visitor) ];
}

void Object::mth_Iterate(introspective::Message &msg) {
  tar::Iterate &iteration = dynamic_cast<md::tar::Iterate&>(msg);
  if (!iteration.started()) {
    iteration.start(this);
  }
  for(;iteration.current!=tar::Iterate::Iterator(); ++iteration.current) {
    if (iteration.count>0) {
      --iteration.count;
    }
    if ( iteration.visitor && iteration.visitor(iteration.current, iteration.result) < 1)
      return;
  }
}

//**** Iterate ****************************************************************

Iterate::Iterate(void *result, int count) : msg::IterateWith< Iterator >(result,count),in(new Callback) {
}

Iterate::Iterate(Visitor visitor, void *result, int count) : msg::IterateWith< Iterator >(visitor,result,count),in(new Callback) {
}

void Iterate::start(Canonical *object) {
  msg::IterateWith<Iterator>::start(object);
  this->in.open(path.string());
  this->current(in);
}

}

}

extern "C" bool initialize(ClassInfo::Registry *,void * descriptor) {
  using namespace md;
  return Descriptor::initConcrete<Tar,Descriptor::MIME_KEY>(descriptor,DESCRIPTOR);
}
