/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MD_TAR_H
#define MD_TAR_H

#include <archive++/tar.h>
#include <md/messages.h>
#include <md/regularfile.h>

namespace md {

/** tar object(s) namespace. */
namespace tar {

/**
 * USTAR archive format as defined in the POSIX (IEEE P1003.1) standard
 *
 * NEW METHODS
 *
 * - [ Iterate ] -> void*
 */
class Object : public RegularFile {
public:
  /** public and Allocator's file constructor */
  explicit Object(const Path &path);
protected:
  /** derived meta class constructor */
  Object(Descriptor *descriptor,const Path &path);
private:
  void mth_Enumerate(introspective::Message &message);
  void mth_Iterate(introspective::Message &msg);
};

struct Iterate : public msg::IterateWith< arc::tar::Iterator > {
  Iterate(void *result = nullptr ,int count=-1);
  Iterate(Visitor visitor, void *result = nullptr ,int count=-1);
  Iterate & operator () ( Visitor visitor, void *result = nullptr ,int count=-1) {
    msg::IterateWith<Iterator>::operator()(visitor,result,count);
    return *this;
  }
  void start(Canonical *object) override;
  Iterator current;
  arc::tar::Reader in;
};

}

using Tar = tar::Object;

}

#endif // MD_TAR_H
