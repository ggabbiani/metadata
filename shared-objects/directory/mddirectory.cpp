/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <eel/eel.h>
#include "md/directory.h"
#include "md/properties.h"

#include "debug/trace.h"

using namespace std;
using namespace eel;

namespace fs	= boost::filesystem;
namespace imsg  = introspective::msg;

namespace {

md::Descriptor *DESCRIPTOR = nullptr;

}

namespace md {

namespace directory {

Object::Object(const Path &info): System(DESCRIPTOR,info) {
  using std::placeholders::_2;
  bind< msg::Enumerate  >(std::bind(&Object::mth_Enumerate, this,_2));
  bind< msg::Iterate >(std::bind(&Object::mth_Iterate,this,_2));
}

void Object::mth_Enumerate(introspective::Message &message) {
  using md::msg::Enumerate;
  Enumerate         &msg    = dynamic_cast< Enumerate& >(message);
  Enumerate::Result  result = *msg.result;
  Factory        &factory = Factory::singleton();
  // to this visitor a nullptr is passed as data, result are accessed directly
  // through lambda enclosure.
  Iterate::Visitor visitor = [ &result,&factory ] (const Iterate::Iterator &i,void *data) -> bool {
    TR_FUNC;
    // a directory iterator holds a boost::filesystem::directory_entry
    auto path = i->path();
    // ask factory to create an object from path
    Canonical *optr = factory.create(path);
    // assign the object to a std::unique_ptr
    introspective::proxy::UniquePtr< Canonical > object(optr);
    // push back the result
    result.push_back(std::move(object));
    return true;
  };
  (*this) [ directory::Iterate(visitor) ];
}

void Object::mth_Iterate(introspective::Message &msg) {
  directory::Iterate &iteration = dynamic_cast<directory::Iterate&>(msg);
  if (!iteration.started()) {
    iteration.start(this);
  }
  for(;iteration.current!=Iterate::Iterator(); ++iteration.current) {
    if (iteration.count>0) {
      --iteration.count;
    }
    if ( iteration.visitor && iteration.visitor(iteration.current, iteration.result) < 1)
      return;
  }
}

void Iterate::start(Canonical *object) {
  msg::IterateWith<Iterator>::start(object);
  current = Iterate::Iterator(path);
}

}

}

extern "C" bool initialize(tsl::rtx::ClassInfo::Registry *,void *descriptor) {
  using namespace md;
  return Descriptor::initConcrete<Directory,Descriptor::MIME_KEY>(descriptor,DESCRIPTOR);
}
