/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MD_DIRECTORY_H
#define MD_DIRECTORY_H

#include <md/messages.h>
#include <md/system.h>

namespace md {

namespace directory {

/**
 * directory is the most common Unix special file
 *
 * NEW METHODS
 *
 * - [ msg::Browse  ] -> msg::CanonicalCollection
 * - [ msg::Iterate ] -> void*
 */
class Object : public System {
public:
  /** public constructor */
  explicit Object(const Path &path);
protected:
  /** derived meta class constructor */
  Object(Descriptor *descriptor,const Path &info) : System(descriptor,info) {
  }
private:
  void mth_Enumerate(introspective::Message &message);
  void mth_Iterate(introspective::Message &msg);
};

struct Iterate : public msg::IterateWith< boost::filesystem::directory_iterator > {
  Iterate(void *result = nullptr ,int count=-1) : IterateWith< Iterator >(result,count) {
  }
  Iterate(Visitor visitor, void *result = nullptr ,int count=-1) : IterateWith< Iterator >(visitor,result,count) {
  }
  Iterate & operator () ( Visitor visitor, void *result = nullptr ,int count=-1) {
    msg::IterateWith<Iterator>::operator()(visitor,result,count);
    return *this;
  }
  void start(Canonical *object) override;
  Iterator current;
};

}

using Directory = directory::Object;

}

#endif // MD_DIRECTORY_H
