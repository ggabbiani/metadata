/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "md/messages.h"
#include "md/fifo.h"

#include "debug/trace.h"

using namespace std;

namespace {

md::Descriptor* DESCRIPTOR = nullptr;

}

namespace md {

namespace fifo {

Object::Object(const Path &info): System(DESCRIPTOR,info) {
}

}

}

extern "C" bool initialize(tsl::rtx::ClassInfo::Registry *,void * descriptor) {
  using namespace md;
  return Descriptor::initConcrete<Fifo,Descriptor::MIME_KEY>(descriptor,DESCRIPTOR);
}
