/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016  Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "md/regularfile.h"

namespace md {

/** concrete namespace for ELF executables */
namespace elf {

/** properties for elf objects */
namespace property {

/**
 * ELF_CLASS (unsigned char) set to either 1 or 2 to signify 32- or 64-bit
 * format, respectively.
 *
 */
constexpr const char * CLASS = "ELF_CLASS";

/**
 * ELF_DATA (unsigned char) set to either 1 or 2 to signify little or big
 * endianness, respectively.
 */
constexpr const char * DATA = "ELF_DATA";

/**
 * ELF_VERSION (unsigned) Set to 1 for the original version of ELF.
 */
constexpr const char * VERSION = "ELF_VERSION";

/**
 * ELF_OSABI (unsigned char) Identifies the target operating system ABI.
 *
 * Value |  ABI
 * ----- | ----------
 * 0x00  | System V
 * 0x01  | HP-UX
 * 0x02  | NetBSD
 * 0x03  | Linux
 * 0x06  | Solaris
 * 0x07  | AIX
 * 0x08  | IRIX
 * 0x09  | FreeBSD
 * 0x0C  | OpenBSD
 * 0x0D  | OpenVMS
 * 0x0E  | NSK operating system
 * 0x0F  | AROS
 * 0x10  | Fenix OS
 * 0x11  | CloudABI
 * 0x53  | Sortix
 */
constexpr const char * OSABI = "ELF_OSABI";

/**
 * ELF_ABIVERSION (unsigned char) Further specifies the ABI version. Its
 * interpretation depends on the target ABI. Linux kernel (after at least 2.6)
 * has no definition of it.[5] In that case, offset and size of EI_PAD are 8.
 */
constexpr const char * ABIVERSION = "ELF_ABIVERSION";

/**
 * ELF_TYPE (unsigned short) 1, 2, 3, 4 specify whether the object is
 * relocatable, executable, shared, or core, respectively.
 */
constexpr const char * TYPE = "ELF_TYPE";

/**
 * ELF_MACHINE (unsigned short) Specifies target instruction set
 * architecture.
 *
 * Value | ISA
 * ----- | --------
 * 0x00  | No specific instruction set
 * 0x02  | SPARC
 * 0x03  | x86
 * 0x08  | MIPS
 * 0x14  | PowerPC
 * 0x28  | ARM
 * 0x2A  | SuperH
 * 0x32  | IA-64
 * 0x3E  | x86-64
 * 0xB7  | AArch64
 */
constexpr const char * MACHINE = "ELF_MACHINE";

}
/**
 * ELF (Executable and Linkable Format).
 */
class Object : public RegularFile {
protected:
  /** derived meta class constructor */
  Object(Descriptor *descriptor,const Path &path);
};

}

using ELF = elf::Object;

}
