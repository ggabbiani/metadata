/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MetaData is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <elfio/elfio.hpp>

#include "md/properties.h"
#include "md/elf.h"

using namespace std;

namespace {

md::Descriptor* DESCRIPTOR = nullptr;

const unordered_map< unsigned, const char * > CLASS = {
  { 0x01, "32-bit" },
  { 0x02, "64-bit" }
};

const unordered_map< unsigned, const char * > DATA = {
  { 0x01, "LSB" },
  { 0x02, "MSB" }
};

const unordered_map< unsigned, const char * > OSABI = {
  { 0x00, "System V" },
  { 0x01, "HP-UX" },
  { 0x02, "NetBSD" },
  { 0x03, "Linux" },
  { 0x06, "Solaris" },
  { 0x07, "AIX" },
  { 0x08, "IRIX" },
  { 0x09, "FreeBSD" },
  { 0x0C, "OpenBSD" },
  { 0x0D, "OpenVMS" },
  { 0x0E, "NSK operating system" },
  { 0x0F, "AROS" },
  { 0x10, "Fenix OS" },
  { 0x11, "CloudABI" },
  { 0x53, "Sortix" }
};

const unordered_map< unsigned, const char * > TYPE = {
  { 0x01, "relocatable" },
  { 0x02, "executable"  },
  { 0x03, "shared"      },
  { 0x04, "core"        }
};

const unordered_map< unsigned, const char * > MACHINE = {
  { 0x00, "No specific instruction set" },
  { 0x02, "SPARC"                       },
  { 0x03, "x86"                         },
  { 0x08, "MIPS"                        },
  { 0x14, "PowerPC"                     },
  { 0x28, "ARM"                         },
  { 0x2A, "SuperH"                      },
  { 0x32, "IA-64"                       },
  { 0x3E, "x86-64"                      },
  { 0xB7, "AArch64"                     }
};

template < typename MAP >
string decode(unsigned key,const MAP &map) {
  return map.count(key) ? map.at(key) : "?!";
}

}

namespace md {

namespace elf {

Object::Object(Descriptor *descriptor,const Path &path) : RegularFile(descriptor,path) {
  ELFIO::elfio reader;
  if (not reader.load(this->name())) {
    throw eel::RuntimeError(eel::attribute::what("problemi"),EELC_DIAG);
  }
  define(property::CLASS,      reader.get_class());
  define(property::DATA,       reader.get_encoding());
  define(property::VERSION,    reader.get_version());
  define(property::OSABI,      reader.get_os_abi());
  define(property::ABIVERSION, reader.get_abi_version());
  define(property::TYPE,       reader.get_type());
  define(property::MACHINE,    reader.get_machine());

  define(md::property::DESCRIPTION,
         "ELF "
         + decode(reader.get_class(),CLASS)
         + ' '
         + decode(reader.get_encoding(),DATA)
         + ' ' + decode(reader.get_type(),TYPE)
         + ", " + decode(reader.get_machine(),MACHINE)
         + ", version " + boost::lexical_cast<string>(reader.get_version())
         + " (" + decode(reader.get_os_abi(),OSABI) + ')'
        );
}

}

}

extern "C" bool initialize(tsl::rtx::ClassInfo::Registry *,void *descriptor) {
  using namespace md;
  return Descriptor::initAbstract<ELF>(descriptor,DESCRIPTOR);
}
