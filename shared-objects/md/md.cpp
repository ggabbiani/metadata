/*
 * This file is part of C++ Introspective library project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <boost/spirit/include/qi.hpp>

#include <eel/eel.h>
#include <tsl/json_archive.h>
#include <tsl/extensions.h>

// #define DOTRACE
#include "debug/trace.h"

#include "md_private.h"
#include "md/md.h"
#include "md/messages.h"
#include "md/properties.h"

using namespace introspective;
using namespace md;
using namespace std;

namespace ea   = eel::attribute;
namespace fs   = boost::filesystem;
namespace pa   = tsl::parser::attribute;
namespace imsg = introspective::msg;

namespace md {

/** FIXME unefficient
 * setup the absolute canonical name (without symbolic link resolution)
 */
string canonical(const fs::path &p) {
  deque<string> q;
  fs::path absolute=fs::absolute(p);
  for (auto i: absolute) {
    if (i == ".") {
    } else if (i == "..") {
      q.pop_back();
    } else {
      q.push_back(i.string());
    }
  }
  string result;
  for(auto i=q.begin()+1;i!=q.end();++i) {
    result += '/' + *i;
  }
  return result.empty() ? string("/") : result;
}

}

//**** Canonical **************************************************************

Canonical::Canonical(Descriptor *descriptor,const fs::path &p) : Object(md::canonical(p).c_str()) {
  descriptor->adopt(this);
  define(property::REAL_PATH,fs::canonical(p).string());
}

void Canonical::serialize(tsl::OArchive& out) const {
  introspective::Object::serialize(out);
}

void Canonical::deserialize(tsl::IArchive& in) {
  introspective::Object::deserialize(in);
}

//**** Descriptor *************************************************************

Descriptor::Descriptor() {
  using std::placeholders::_1;
  using std::placeholders::_2;
  /*TR_FUNC;*/
  bind<msg::Find>(std::bind(&Descriptor::cm_Find,this,_2));
}

Descriptor::~Descriptor() {
  /*TR_FUNC;*/
}

void Descriptor::serialize(tsl::OArchive& out) const {
  Class::serialize(out);
}

void Descriptor::deserialize(tsl::IArchive& in) {
  /*TR_FUNC;*/
  Class::deserialize(in);
}

void Descriptor::cm_Find(Message &msg) {
  msg::Find  &message   = dynamic_cast<msg::Find&>(msg);
  assert(message.recipient());
  Descriptor &recipient = dynamic_cast<Descriptor&>(*message.recipient());
  if (this->name() == message.name) {
    message.result = this;
    return;
  }
  for(auto child: children_) {
    if ((*child) [message] != nullptr) {
      return;
    }
  }
}

//**** Factory ****************************************************************

Factory &Factory::singleton() {
//   TR_FUNC;
  static Factory factory;
  return factory;
}


Factory::Factory() {
//   TR_FUNC;
}

Factory::~Factory() {
//   TR_FUNC;
  // root reset triggers the descriptors destruction BEFORE closing the 
  // plugins through the plugins_ destructor: DO NOT CHANGE THIS BEHAVIOUR
  delete root_;
}

Factory& Factory::configure(const char *fname) {
  TR_FUNC;
  Factory &factory = Factory::singleton();
  const char *config = fname ? fname : getenv("MD_CONFIG");
#ifndef NDEBUG
  const char *libpath = getenv("LD_LIBRARY_PATH");
#endif // NDEBUG
  if (!config)
    config = MD_CONFIG;
  eel::ContextAttribute a_archive(pa::archive(config));
  // deserialization
  ifstream stream(config);
  if (stream.fail()) {
    throw eel::Errno(EELC_DIAG);
  }
  stream.unsetf(ios::skipws);
  tsl::IJsonArchive archive(stream);
  {
    Descriptor::Proxy proxy;
//     archive.setDebug(true);
    archive & factory;
  }
  factory.initPlugins();
  return factory;
}

void Factory::serialize(tsl::OArchive& out) const {
  out & tsl::mt("root",root_);
}

void Factory::deserialize(tsl::IArchive& in) {
//   TR_FUNC;
  in & root_;
}

Descriptor& Factory::describe(const Path &path, const char *folk) {
  namespace qi = boost::spirit::qi;
//   TR_FUNC;
//   TR_MSG(path.string());
  EEL_PRECOND_M(fs::exists(path),"Not existing path '" + path.string() + '\'');
  if (const char *desc = magic.describe(path.c_str())) {
    string s(desc);
    string mime,cset;
    bool r = qi::phrase_parse(s.begin(),s.end(),*(~qi::char_(';')) >> ';' >> qi::lit("charset=") >> *qi::char_,boost::spirit::ascii::space,mime,cset);
//     EEL_ASSERT_M(r,"from " + s + " mime = '" + mime + "' cset = '" + cset + '\'');
    assert(r);
    Descriptor *candidate = nullptr;
    auto i=mime_.find(mime);
    auto j=charset_.find(cset);
    if (i != mime_.end()) {
      candidate = i->second;
    } else if (j != charset_.end()) {
      candidate = j->second;
    }
    if (candidate) {
      if (folk) {
        Descriptor *ancestor = find(folk);
        EEL_PRECOND_M(ancestor,"Ancestor '"+string(folk)+"' not found");
        if ((*candidate) [ introspective::msg::Is(*ancestor) ]) {
          return *candidate;
        }
      } else {
        return *candidate;
      }
    }
  }
  throw eel::UnsupportedFeature("data",path.string(),EELC_DIAG);
}


Canonical *Factory::create(const Path &path,const char *folk) {
  Descriptor &descriptor = describe(path,folk);
  return descriptor.abstract() ? nullptr : descriptor.allocator_(path);
}

Descriptor* Factory::find(const char *name) {
  return root_ ? (*root_) [ msg::Find(name) ] : nullptr;
}

void Factory::initPlugins(Descriptor *descriptor) {
  TR_FUNC;
  if (!descriptor)
    descriptor = root_;
  TR_MSG(descriptor->name());
  bool has_lib = (*descriptor)[ imsg::HasProperty(property::LIBRARY) ];
  EEL_ASSERT_M(has_lib ,descriptor->name() + " without " + property::LIBRARY);
  if (has_lib) {
    string soname = (*descriptor) [imsg::GetAs<string>(property::LIBRARY)];
    IoPluginArea io(descriptor);
    plugins().initialize(soname,RTLD_NOW|RTLD_GLOBAL,"initialize",&io);
    TR_MSG(soname);
    if ( io.key == Descriptor::MIME_KEY ) {
      EEL_PRECOND_M( (*descriptor) [ imsg::HasProperty(property::MIME) ],descriptor->name() + " attempts to register a missing MIME") ;
      string key = (*descriptor) [ imsg::GetAs<string>(property::MIME)];
      EEL_PRECOND_M( mime_.find(key) == mime_.end(), descriptor->name() + " attempts to register an already existing MIME (" + key + ')');
      mime_.emplace(key,descriptor);
    }
    if ( io.key == Descriptor::CHARSET_KEY ) {
      EEL_PRECOND_M( (*descriptor) [ imsg::HasProperty(property::CHARSET) ],descriptor->name() + " attempts to register a missing CHARSET") ;
      string key = (*descriptor) [ imsg::GetAs<string>(property::CHARSET)];
      EEL_PRECOND_M( charset_.find(key) == charset_.end(), descriptor->name() + " attempts to register an already existing CHARSET (" + key + ')');
      charset_.emplace(key,descriptor);
    }
  }
  if (descriptor->hasParent()) {
    descriptor->define(property::SYS_PATH,PUBLIC,descriptor->parent() [imsg::GetAs<string>(property::SYS_PATH)] + '/' + descriptor->name());
  } else {
    descriptor->define(property::SYS_PATH,PUBLIC,'/'+descriptor->name());
  }
  for(auto child: descriptor->children_) {
    initPlugins(&dynamic_cast<Descriptor&>(*child));
  }
  TR_MSG("ok");
}
