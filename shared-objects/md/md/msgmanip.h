/*
 * This file is part of C++ Introspective library project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MD_MSGMANIP_H
#define MD_MSGMANIP_H

#include <introspective/msgmanip.h>
#include <md/messages.h>

namespace md {

/**
 * Modifies the message iteration count.
 */
inline msg::Iterate::Manipulator< int > count(int c) {
  return msg::Iterate::Manipulator< int >(msg::Iterate::apply_count, c);
}

/**
 * Set the message visitor.
 *
 * A message has to 'visitable', i.e. to have a static member function acting as applicator with the following signature:
 * @code
 * MESSAGE& apply_visitor( MESSAGE& message, Visitor v );
 * @endcode
 *
 * @tparam MESSAGE a message with a visitor applicator
 */
template < typename MESSAGE,typename VISITOR >
inline introspective::Message::Manipulator< MESSAGE,VISITOR > visitor(VISITOR v) {
  return introspective::Message::Manipulator< MESSAGE,VISITOR >(MESSAGE::apply_visitor, v);
}

}

#endif // MD_MSGMANIP_H
