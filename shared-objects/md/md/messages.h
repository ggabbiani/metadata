/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MD_MESSAGES_H
#define MD_MESSAGES_H

#include <md/md.h>
#include <introspective/proxy.h>

namespace md {

/**
  * namespace for MetaData messages
  */
namespace msg {

/**
 * find a Descriptor with top-down algorithm
 *
 * Signature: [ Find ] -> Descriptor*
 *
 */
struct Find : public introspective::msg::Adaptor< Find,Descriptor* > {
public:
  explicit Find(const char *name) : name(name) {
  }
  /** the name to search the Descriptor for */
  const char * const name;
};

template <typename ITERATOR>
using Visitor = std::function<bool(const ITERATOR &,void*)>;

/**
 * collection of Canonical objects
 */
using CanonicalCollection = std::list< introspective::proxy::UniquePtr< Canonical > >;

/**
 * Enumerate message should be implemented by every object container such as
 * Tar and Directory.
 *
 * Signature: [ Enumerate ] -> CanonicalCollection&
 */
struct Enumerate : public introspective::msg::Adaptor< Enumerate,CanonicalCollection* > {
  using Result = CanonicalCollection&;
  Enumerate(Result result) {
    this->result = &result;
  }
  Result operator * () {
    return *result;
  }
};

/**
 * Abstract message for iterable objects.
 *
 * Signature: [ Iterate ] -> void*
 */
class Iterate : public introspective::msg::Adaptor< Iterate,void* > {
public:
  template < typename T>
  class Manipulator : public introspective::Message::Manipulator< Iterate,T > {
  public:
    using Action = typename introspective::Message::Manipulator< Iterate,T >::Action;
    Manipulator(Action action, T value) : introspective::Message::Manipulator< Iterate,T >(action,value) {
    }
  };
  Iterate(void *result=nullptr) {
    this->result = result;
  }
  virtual void start(md::Canonical *object);
  virtual bool started();
  int  count = -1;
  Path path;
  /**
   * count applicator
   */
  static Iterate& apply_count(Iterate &message,int c) {
    message.count = c;
    return message;
  }
};

/**
 * Concrete base for iterate messages.
 *
 * Signature: [ Iterate ] -> void*
 *
 * @tparam ITERATOR the interator type used for iterate inside the object
 */
template <typename ITERATOR>
class IterateWith : public Iterate {
public:
  using Iterator = ITERATOR;
  using Visitor  = msg::Visitor<ITERATOR>;
  using Entry    = typename ITERATOR::value_type;
  /** default visitor, actually do nothing but iterate till end */
  static bool skip(const ITERATOR&,void *) {
    return true;
  }
  IterateWith(void *result = nullptr ,int count=-1) : Iterate(result) {
  }
  IterateWith(Visitor visitor, void *result = nullptr ,int count=-1) : Iterate(result) {
    this->visitor = visitor;
    this->count   = count;
  }
  IterateWith & operator () ( Visitor visitor, void *result = nullptr ,int count=-1 ) {
    this->visitor = visitor;
    this->result  = result;
    this->count   = count;
    return *this;
  }
  /**
   * applicator for visitor.
   */
  template < typename MESSAGE >
  static MESSAGE & apply_visitor( MESSAGE &message,Visitor v) {
    message.visitor = v;
    return message;
  }
  Visitor visitor = skip;
};

/** parameter for countable messages */
// template < typename TYPE >
// class count {
//   template <typename MESSAGE>
//   friend MESSAGE & operator << ( MESSAGE &msg, count cnt ) {
//     return cnt(msg);
//   }
// public:
//   count(TYPE v) : value(v) {}
//   template < typename MESSAGE >
//   MESSAGE & operator () ( MESSAGE &message) const {
//     message.count = value;
//     return message;
//   }
// private:
//   TYPE value;
// };

/** parameter for visitable messages */
// template < typename FUNC >
// class visitor {
//   template <typename MESSAGE>
//   friend MESSAGE & operator << ( MESSAGE &msg, visitor v ) {
//     return v(msg);
//   }
// public:
//   visitor( FUNC v) : value(v) {}
//   template <typename M>
//   M& operator () (M &message) const {
//     message.visitor = value;
//     return message;
//   }
// private:
//   FUNC value;
// };

}

}

#endif // MD_MESSAGES_H
