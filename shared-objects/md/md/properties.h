/**
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MD_PROPERTIES_H
#define MD_PROPERTIES_H

namespace md {

/**
 * general properties namespace
 */
namespace property {

/**
 * MD_DESCRIPTION (std::string) description of the metadata as written in the configuration file
 */
constexpr const char * DESCRIPTION = "MD_DESCRIPTION";
/**
 * MD_LIBRARY (std::string) the file part of the shared object implementation of a meta data
 */
constexpr const char * LIBRARY     = "MD_LIBRARY";
/**
 * MD_SYS_PATH (std::string) the abstract path of a descriptor inside the meta data hierarchy
 */
constexpr const char * SYS_PATH    = "MD_SYS_PATH";
/**
 * MD_REAL_PATH (std::string) the name of an instance corresponding to the canonical path WITH
 * symbol link resolution.
 */
constexpr const char * REAL_PATH   = "MD_REAL_PATH";

/**
 * MD_MIME (std::string) mime tipe.
 */
constexpr const char * MIME        = "MD_MIME";

/**
 * MD_CHARSET (std::string) charset used for indexing the plugin search.
 */
constexpr const char * CHARSET     = "MD_CHARSET";

}

}

#endif // MD_PROPERTIES_H
