/*
 * This file is part of C++ Introspective library project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef META_DATA_H
#define META_DATA_H

#include <boost/regex.hpp>
#include <magic.h>
#include <tsl/serializable.h>

#define DOTRACE
#include "debug/trace.h"
#include <introspective/introspective.h>

extern "C" bool initialize(tsl::rtx::ClassInfo::Registry *,void *);

namespace md {

using Id   = introspective::TypeIndex;
using Path = boost::filesystem::path;

class Canonical;

class Descriptor : public introspective::Class {
  friend bool ::initialize(tsl::rtx::ClassInfo::Registry *,void *);
  friend class Factory;
public:
  using Proxy     = tsl::rtx::ClassInfo::Proxy<Descriptor>;
  using Allocator = std::function<Canonical *(const Path &)>;
  enum Key {
    NO_KEY      = 0,
    MIME_KEY    = 1,
    CHARSET_KEY = 2
  };
  /** destructor */
  virtual ~Descriptor();
  Descriptor();
  bool abstract() const {
    return allocator_ ? false : true;
  }
  /**
   * type id of the instances.
   * FIXME really needed?
   */
  Id instanceId;
protected:
  void serialize(tsl::OArchive& out) const override;
  void deserialize(tsl::IArchive& in) override;
private:
  /**
   * initialises an abstract class descriptor.
   */
  template <typename OBJECT,Key KEY=NO_KEY>
  static bool initAbstract(void *in,Descriptor *&out);
  /**
   * initialises a concrete class descriptor.
   */
  template <typename OBJECT,Key KEY=NO_KEY>
  static bool initConcrete(void *in,Descriptor *&out,Allocator allocator);
  /**
   * initialises a concrete class descriptor with default allocator.
   */
  template <typename OBJECT,Key KEY=NO_KEY>
  static bool initConcrete(void *in,Descriptor *&out);
  /** strong recognition method. Check the regular expression if any AND the inspector. */
  unsigned recognize(const Path &path) const;
  void cm_Find(introspective::Message &base);
  Allocator allocator_;
};

/**
 * It's the base for metadata objects.
 * The name of an instance corresponds to the canonical path i.e. the
 * absolute path leading to the decribe object WITHOUT symbol link resolution.
 *
 * NEW PROPERTIES
 *
 * - property::REAL_PATH
 */
class Canonical : public introspective::Object {
protected:
  /** derived meta class constructor */
  Canonical(Descriptor *descriptor,const Path &p);
  void serialize(tsl::OArchive& out) const override;
  void deserialize(tsl::IArchive& in) override;
};

struct Magic {
  Magic(int flags=MAGIC_MIME) {
    cookie = magic_open(flags);
    if (!cookie) {
      throw eel::RuntimeError(eel::attribute::what(magic_error(cookie)),EELC_DIAG);
    }
    if (magic_load(cookie,nullptr) != 0) {
      throw eel::RuntimeError(eel::attribute::what(magic_error(cookie)),EELC_DIAG);
    }
  }
  ~Magic() {
    magic_close(cookie);
  }
  const char *describe(const char *file) {
    return magic_file(cookie,file);
  }
  magic_t cookie = nullptr;
};

/**
 * Main Factory class for object description and creation.
 */
class Factory : public virtual tsl::Serializable {
public:
  /**
   * this struct is passed to the plugin with the descriptor pointer
   * initialized, and filled with the proper key value by the plugin init
   * function.
   */
  struct IoPluginArea {
    IoPluginArea(Descriptor *descriptor) : descriptor(descriptor) {
    }
    Descriptor * const descriptor;
    Descriptor::Key key;
  };
  /** Build a Factory from the configuration file passed.
   * The config file name evaluation order is:
   * -# fname parameter
   * -# MD_CONFIG variable
   * -# canonical position.
   * @return the Factory instance
   */
  static Factory& configure(const char *fname=nullptr);
  /**
   * return the class singleton
   */
  static Factory& singleton();
  /** Examines a file and return its Descriptor.
   * @param path the path to find the Descriptor for
   * @param folk name of the meta-data ancestor from which starting the Descriptor search
   * @return the meta data Descriptor if success, null otherwise.
   * @note The returned Descriptor pointer is internally allocated, client code
   * should __NEVER__ try to free it.
   */
  Descriptor& describe(const Path &path, const char *folk=nullptr);
  /** given a meta data id attempts to find the Descriptor from the meta data DB.
   * @param id the meta data id
   * @return the meta data Descriptor if success, null otherwise.
   * @note The returned Descriptor pointer is internally allocated, client code
   * should __NEVER__ try to free it.
   */
  Descriptor *find(const char *name);
  /** Object creation from a file.
   * @param path file system path
   * @param folk ancestor name from which the object has to derive
   * @return the Object meta data if success, NULL otherwise.
   */
  Canonical *create(const Path &path, const char *folk=nullptr);
  template <typename T>
  T *create(const Path &path, const char *folk=nullptr) {
    using namespace tsl::rtx;
    using namespace eel;
    auto actual = create(path,folk);
    T *desired = dynamic_cast<T*>(actual);
    if (!desired) {
      throw RuntimeError(attribute::what(std::string("Data type mismatch (got ")+demangle(typeid(*actual))+" instead of "+demangle<T>()+')'),EELC_DIAG);
    }
    return desired;
  }
  tsl::rtx::plugin::Factory &plugins() {
    return plugins_;
  }
protected:
  void serialize(tsl::OArchive& out) const override;
  void deserialize(tsl::IArchive& in) override;
private:
  Factory();
  ~Factory();
  void initPlugins(Descriptor *descriptor=nullptr);
  Descriptor *root_ = nullptr;
  tsl::rtx::plugin::Factory plugins_;
  std::map<std::string,Descriptor*> mime_;
  std::map<std::string,Descriptor*> charset_;
  Magic magic;
};

template <typename OBJECT,Descriptor::Key KEY>
inline bool Descriptor::initAbstract(void *in, md::Descriptor *&out ) {
  Factory::IoPluginArea *io = (Factory::IoPluginArea*)in;
  out                       = io->descriptor;
  out->instanceId           = typeid(OBJECT);
  io->key                   = KEY;
  return true;
}

template <typename OBJECT,Descriptor::Key KEY>
inline bool Descriptor::initConcrete(void *in, md::Descriptor *&out, md::Descriptor::Allocator allocator ) {
  TR_FUNC;
  Factory::IoPluginArea *io = (Factory::IoPluginArea*)in;
  TR_MSG("out descriptor " << out);
  out                       = io->descriptor;
  TR_MSG("out descriptor " << out);
  out->instanceId           = typeid(OBJECT);
  out->allocator_           = allocator;
  io->key                   = KEY;
  return true;
}

template <typename OBJECT,Descriptor::Key KEY>
inline bool Descriptor::initConcrete(void *in, md::Descriptor *&out) {
  return initConcrete<OBJECT,KEY>(in,out,[] (const Path &path) {return new OBJECT(path);});
}

}

#endif // META_DATA_H
