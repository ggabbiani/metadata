/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MD_TEXT_H
#define MD_TEXT_H

#include <fstream>

#include <md/messages.h>
#include <md/regularfile.h>

namespace md {

/** abstract meta-class for text files */
namespace text {

// from http://stackoverflow.com/questions/2291802/is-there-a-c-iterator-that-can-iterate-over-a-file-line-by-line
namespace detail {
  class Line : public std::string {
    friend std::istream & operator>>(std::istream & is, Line & line) {
      return std::getline(is, line);
    }
  };
}

/** namespace for text object properties */
namespace property {

/**
  * TXT_CONTENT (string) the text content in utf8 coding.
  * A read attempts of the property when not initialized,
  * triggers a Read method on the Text object.
  */
constexpr const char * CONTENT = "TXT_CONTENT";

}

/**
 * Text document data type, the inner format is UTF-8.
 *
 * NEW PROPERTIES
 *
 * - text::property::CONTENT
 *
 * NEW METHODS
 *
 * - [ Iterate ] -> void*
 * - [ Read ] -> std::string
 */
class Object : public RegularFile {
protected:
  /** derived meta class constructor */
  Object(Descriptor *descriptor,const Path &info);
private:
  /** manages a md::text::Iterate message */
  void mth_Iterate(introspective::Message &msg);
  /** manages a md::text::Read message updating the property::CONTENT */
  void mth_Read(introspective::Message &msg);
};

struct Iterate : public msg::IterateWith< std::istream_iterator<detail::Line> > {
  Iterate(void *result = nullptr ,int count=-1) : msg::IterateWith<Iterator>(result,count) {
  }
  Iterate(Visitor visitor, void *result = nullptr ,int count=-1) : msg::IterateWith<Iterator>(visitor,result,count) {
  }
  Iterate & operator () ( Visitor visitor, void *result = nullptr ,int count=-1) {
    msg::IterateWith<Iterator>::operator()(visitor,result,count);
    return *this;
  }
  void start(Canonical *object) override;
  Iterator      current;
  std::ifstream in;
};

/**
 * Read a text object
 *
 * Signature: [ Read ] -> std::string
 *
 */
struct Read : public introspective::msg::Adaptor<Read,std::string> {
  std::string content;
};

}

using Text = text::Object;

}

#endif // MD_TEXT_H
