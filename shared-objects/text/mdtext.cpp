/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>
#include <string>

#include "md/messages.h"
#include "md/text.h"

using namespace std;
using namespace tsl::rtx;

namespace {

md::Descriptor* DESCRIPTOR = nullptr;

}

namespace md {

namespace text {

//**** Object *****************************************************************

Object::Object(Descriptor *descriptor,const Path &info) : RegularFile(descriptor,info) {
  using std::placeholders::_2;
  // add a public object method to the text abstract meta class for text iterate
  bind< msg::Iterate >(std::bind(&Object::mth_Iterate,this,_2));
  bind< Read >(std::bind(&Object::mth_Read,this,_2));
  bind< introspective::msg::Notify >(
    [] (introspective::Object &receiver,introspective::Message &msg) {
      introspective::msg::Notify &message = dynamic_cast<introspective::msg::Notify&>(msg);
      introspective::Property    *property = message.result;
      if ((property->value.type() == typeid(tsl::Variant::Void)) and (message.event == introspective::Property::READ)) {
        receiver [ Read() ];
      }
    }
  );
  declare(property::CONTENT);
  notify(property::CONTENT,introspective::Property::READ,*this);
}

void Object::mth_Iterate(introspective::Message &m) {
  text::Iterate &iteration  = dynamic_cast<text::Iterate&>( m );
  EEL_PRECOND_M(( iteration.in.rdstate() & (std::ifstream::failbit | std::ifstream::badbit )) == 0,"bad stream status");
  if (!iteration.started()) {
    iteration.start(this);
  }
  if (!iteration.in.is_open()) {
    iteration.in.open(this->name());
  }
  text::Iterate::Iterator end;
  for(;(iteration.count != 0) && (iteration.current!=end);++iteration.current) {
    if ( iteration.count>0) {
      --iteration.count;
    }
    if ( iteration.visitor && !iteration.visitor(iteration.current, iteration.result)) {
      return;
    }
  }
}

void Object::mth_Read(introspective::Message &msg) {
  text::Read &message = dynamic_cast<text::Read&>(msg);
  ifstream file(name());
  file.seekg(0,ios::end);
  message.content.reserve(file.tellg());
  file.seekg(0,ios::beg);
  message.content.assign((istreambuf_iterator<char>(file)),istreambuf_iterator<char>());
  (*this) [ introspective::msg::Set(property::CONTENT,message.content) ];
}

//**** Iterate ****************************************************************

void Iterate::start(Canonical *object) {
  msg::IterateWith<Iterator>::start(object);
  in.open(path.c_str());
  current = Iterator(in);
}

}

}

extern "C" bool initialize(ClassInfo::Registry *,void *descriptor) {
  using namespace md;
  return Descriptor::initAbstract<Text>(descriptor,DESCRIPTOR);
}
