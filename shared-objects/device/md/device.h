/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MD_DEVICE_H
#define MD_DEVICE_H

#include <string>

#include <md/system.h>

namespace md {

/** abstract meta-class for system devices */
namespace device {

/** namespace for device properties */
namespace property {

/**
 * DEV_MAJOR (int) major device number
 */
constexpr const char * MAJOR = "DEV_MAJOR";

/**
 * DEV_MINOR (int) minor device number
 */
constexpr const char * MINOR = "DEV_MINOR";

}

/**
 * Abstract class defining a system device, i.e. a block or character one.
 *
 * NEW PROPERTIES
 *
 * - md::device::property::MAJOR
 * - md::device::property::MINOR
 */
class Object : public System {
protected:
  /** derived meta class constructor */
  Object(Descriptor *descriptor,const Path &path);
};

}

using Device = device::Object;

}

#endif
