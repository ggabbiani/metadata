/*
 * This file is part of MetaData project.
 * Copyright (C) 2005-2016 Giampiero Gabbiani <giampiero@gabbiani.org>
 *
 * MetaData is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Introspective is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MetaData.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/stat.h>
#include <tsl/rtx.h>

#include "md/device.h"

using namespace std;

namespace {

md::Descriptor* DESCRIPTOR = nullptr;

}

namespace md {

namespace device {

//**** Object *****************************************************************

Object::Object(Descriptor *descriptor, const Path &path): System(descriptor,path) {
  struct stat info;
  if (lstat(path.c_str(),&info) == -1) {
    throw eel::Errno(EELC_DIAG);
  }
  define(property::MAJOR,(int)(info.st_rdev >> 8)  );
  define(property::MINOR,(int)(info.st_rdev & 0xff));
}

}

}

extern "C" bool initialize(tsl::rtx::ClassInfo::Registry *,void *descriptor) {
  using namespace md;
  return Descriptor::initAbstract<Device>(descriptor,DESCRIPTOR);
}
